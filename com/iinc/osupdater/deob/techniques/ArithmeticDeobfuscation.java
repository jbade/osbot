package com.iinc.osupdater.deob.techniques;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.util.InsnPrinter;

public class ArithmeticDeobfuscation implements DeobfuscationTechnique {

	@SuppressWarnings("unchecked")
	@Override
	public boolean run(HashMap<String, ClassNode> classes, boolean print) {
		int count = 0;
		for (ClassNode cn : classes.values())
			for (MethodNode mn : (Iterable<MethodNode>) cn.methods) {
				int c = reorder(mn);
				// if (c > 0)
				// System.out.println(cn.name + "." + mn.name + " " + c);
				count += c;
			}

		if (print)
			System.out.println(String.format("Reordered %d arithmetic expressions.", count));
		return count == 0;
	}

	static Set<Integer> ends = new HashSet<Integer>();
	static {
		ends.add(Opcodes.IMUL);
		ends.add(Opcodes.DMUL);
		ends.add(Opcodes.FMUL);
		ends.add(Opcodes.LMUL);
		ends.add(Opcodes.DADD);
		ends.add(Opcodes.FADD);
		ends.add(Opcodes.IADD);
		ends.add(Opcodes.LADD);

		// ends.add(Opcodes.DDIV);
		// ends.add(Opcodes.FDIV);
		// ends.add(Opcodes.IDIV);
		// ends.add(Opcodes.LDIV);
		//
		// ends.add(Opcodes.DREM);
		// ends.add(Opcodes.FREM);
		// ends.add(Opcodes.IREM);
		// ends.add(Opcodes.LREM);
		//
		// ends.add(Opcodes.DSUB);
		// ends.add(Opcodes.FSUB);
		// ends.add(Opcodes.ISUB);
		// ends.add(Opcodes.LSUB);
	}

	public int reorder(MethodNode mn) {
		if (mn.instructions.size() == 0)
			return 0;

		ArrayList<AbstractInsnNode> instructions = new ArrayList<>(Arrays.asList(mn.instructions.toArray()));
		int count = 0;

		int i = 0;
		while (true) {
			if (ends.contains(instructions.get(i).getOpcode()))
				break;
			if (++i >= instructions.size()) {
				i = -1;
				break;
			}
		}

		while (i != -1) {
			AbstractInsnNode end = instructions.get(i);
			ExpressionGroup group2 = findGroup(i - 1, instructions, mn);

			if (group2 != null) {
				ExpressionGroup group1 = findGroup(group2.start - 1, instructions, mn);
				if (group1 != null) {
					if (group2.length() > group1.length() || group1.ldc && !group2.ldc) {
						count++;
						ArrayList<AbstractInsnNode> insns = new ArrayList<AbstractInsnNode>();
						for (int j = group1.start; j <= group1.end; j++) {
							// System.out.println(j);
							insns.add(instructions.get(j));
						}

						for (AbstractInsnNode ain : insns) {
							instructions.remove(ain);
							instructions.add(instructions.indexOf(end), ain);
						}
					}
				}
			}

			// find next
			while (true) {
				if (++i >= instructions.size()) {
					i = -1;
					break;
				}
				if (ends.contains(instructions.get(i).getOpcode()))
					break;
			}
		}

		for (AbstractInsnNode insn : mn.instructions.toArray())
			mn.instructions.remove(insn);
		instructions.forEach(e -> mn.instructions.add(e));

		return count;
	}

	private ExpressionGroup findGroup(int end, ArrayList<AbstractInsnNode> instructions, MethodNode mn) {
		boolean cont = true;
		boolean ldc = false;

		int req = 1;

		int j = end;
		for (; j >= 0;) {
			switch (mn.instructions.get(j).getOpcode()) {
			case Opcodes.ALOAD:
				if (--req == 0)
					cont = false;
				break;
			case Opcodes.GETFIELD:
			case Opcodes.ARRAYLENGTH:
				break;
			case Opcodes.GETSTATIC:
				if (--req == 0)
					cont = false;
				break;
			case Opcodes.LDC:
				ldc = true;
			case Opcodes.ILOAD:
			case Opcodes.ICONST_0:
			case Opcodes.ICONST_1:
			case Opcodes.ICONST_2:
			case Opcodes.ICONST_3:
			case Opcodes.ICONST_4:
			case Opcodes.ICONST_5:

			case Opcodes.DCONST_0:
			case Opcodes.DCONST_1:
			case Opcodes.FCONST_0:
			case Opcodes.FCONST_1:
			case Opcodes.FCONST_2:
			case Opcodes.LCONST_0:
			case Opcodes.LCONST_1:
			case Opcodes.BIPUSH:
			case Opcodes.ICONST_M1:
			case Opcodes.SIPUSH:
				if (--req == 0)
					cont = false;
				break;
			// handling this.ci[0] * -1966783104
			case Opcodes.BALOAD:
			case Opcodes.CALOAD:
			case Opcodes.DALOAD:
			case Opcodes.FALOAD:
			case Opcodes.IALOAD:
			case Opcodes.LALOAD:
			case Opcodes.SALOAD:
				req++;
				break;
			case Opcodes.AALOAD:
				req ++;
				break;
			case Opcodes.IMUL:
			case Opcodes.DMUL:
			case Opcodes.FMUL:
			case Opcodes.LMUL:
			case Opcodes.DDIV:
			case Opcodes.FDIV:
			case Opcodes.IDIV:
			case Opcodes.LDIV:
			case Opcodes.DREM:
			case Opcodes.FREM:
			case Opcodes.IREM:
			case Opcodes.LREM:
			case Opcodes.DADD:
			case Opcodes.FADD:
			case Opcodes.IADD:
			case Opcodes.LADD:
			case Opcodes.DSUB:
			case Opcodes.FSUB:
			case Opcodes.ISUB:
			case Opcodes.LSUB:
				ExpressionGroup sub2 = findGroup(j - 1, instructions, mn);
				if (sub2 == null)
					return null;
				ExpressionGroup sub1 = findGroup(sub2.start - 1, instructions, mn);
				if (sub1 == null)
					return null;
				// System.out.println(sub2.start + " " + sub1.end);
				j = sub1.start;
				if (--req == 0)
					cont = false;
				break;
			case Opcodes.D2F:
			case Opcodes.D2I:
			case Opcodes.D2L:
			case Opcodes.F2D:
			case Opcodes.F2I:
			case Opcodes.F2L:
			case Opcodes.I2B:
			case Opcodes.I2C:
			case Opcodes.I2D:
			case Opcodes.I2F:
			case Opcodes.I2L:
			case Opcodes.I2S:
			case Opcodes.L2D:
			case Opcodes.L2F:
			case Opcodes.L2I:
				break;
			case Opcodes.INVOKEDYNAMIC:
			case Opcodes.INVOKEINTERFACE:
			case Opcodes.INVOKESPECIAL:
			case Opcodes.INVOKESTATIC:
			case Opcodes.INVOKEVIRTUAL:
			default:
				return null; // TODO
			// break;
			}

			if (!cont || --j < 0)
				break;
		}

		// System.out.println(new ExpressionGroup(j, end, ldc).toString());
		return new ExpressionGroup(j, end, ldc);
	}

	class ExpressionGroup {
		int start;
		int end;
		boolean ldc = false;

		public ExpressionGroup(int start, int end, boolean ldc) {
			this.start = start;
			this.end = end;
			this.ldc = ldc;
		}

		public int length() {
			return end - start;
		}

		@Override
		public String toString() {
			return "ExpressionGroup [start=" + start + ", end=" + end + ", ldc=" + ldc + "]";
		}

	}

	public int reorderMultiplicationWithConstants(MethodNode method) {
		/*
		 * start at IMUL loop until LDC stop looping if find 2 of these: ALOAD
		 * stop looping if find one that is not ALOAD GETFIELD GETSTATIC
		 */

		ArrayList<AbstractInsnNode> instructions = new ArrayList<>(Arrays.asList(method.instructions.toArray()));
		if (instructions.size() == 0)
			return 0;

		List<Integer> ops = new LinkedList<Integer>();
		ops.add(Opcodes.IMUL);
		ops.add(Opcodes.DMUL);
		ops.add(Opcodes.FMUL);
		ops.add(Opcodes.LMUL);
		ops.add(Opcodes.DDIV);
		ops.add(Opcodes.FDIV);
		ops.add(Opcodes.IDIV);
		ops.add(Opcodes.LDIV);

		int i = 0;
		while (true) {
			if (ops.contains(instructions.get(i).getOpcode()))
				break;
			if (++i >= instructions.size()) {
				i = -1;
				break;
			}
		}

		int count = 0;
		while (i != -1) {
			int aloadCount = 0;
			boolean cont = true;
			int ldc = -1;
			boolean iaload = false;

			for (int j = i - 1; j >= 0 && cont; j--) {
				switch (method.instructions.get(j).getOpcode()) {
				case Opcodes.ALOAD:
					aloadCount++;
					if (aloadCount == 2)
						cont = false;
				case Opcodes.GETFIELD:
				case Opcodes.GETSTATIC:
					break;
				case Opcodes.ILOAD:
				case Opcodes.LDC:
				case Opcodes.ICONST_0:
				case Opcodes.ICONST_1:
				case Opcodes.ICONST_2:
				case Opcodes.ICONST_3:
				case Opcodes.ICONST_4:
				case Opcodes.ICONST_5:
					if (iaload) {
						iaload = false;
						break;
					}
				case Opcodes.DCONST_0:
				case Opcodes.DCONST_1:
				case Opcodes.FCONST_0:
				case Opcodes.FCONST_1:
				case Opcodes.FCONST_2:
				case Opcodes.LCONST_0:
				case Opcodes.LCONST_1:
				case Opcodes.BIPUSH:
				case Opcodes.ICONST_M1:
				case Opcodes.SIPUSH:
					ldc = j;
					cont = false;
					break;
				// handling this.ci[0] * -1966783104
				case Opcodes.BALOAD:
				case Opcodes.CALOAD:
				case Opcodes.DALOAD:
				case Opcodes.FALOAD:
				case Opcodes.IALOAD:
				case Opcodes.LALOAD:
				case Opcodes.SALOAD:
					iaload = true;
					break;
				default:
					cont = false;
					break;
				}
			}

			if (ldc != -1 && ldc != i - 1) {
				instructions.add(i, method.instructions.get(ldc));
				instructions.remove(ldc);
				count++;
			}

			while (true) {
				if (++i >= instructions.size()) {
					i = -1;
					break;
				}
				if (ops.contains(instructions.get(i).getOpcode()))
					break;
			}
		}

		for (AbstractInsnNode insn : method.instructions.toArray())
			method.instructions.remove(insn);
		instructions.forEach(e -> method.instructions.add(e));

		return count;
	}

}
