package com.iinc.osupdater.deob.techniques.cfg.graph;

import java.util.ArrayList;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.deob.techniques.cfg.Block;

/**
 * Handles instructions when constructing the graph.
 */
public abstract class InsnHandler {
	protected ArrayList<Block> blocks;
	protected MethodNode mn;
	protected DirectedPseudograph<Block, DefaultEdge> graph;

	public InsnHandler(ArrayList<Block> blocks, MethodNode mn, DirectedPseudograph<Block, DefaultEdge> graph) {
		this.blocks = blocks;
		this.mn = mn;
		this.graph = graph;
	}

	/**
	 * Handles the instruction by appropriately adding edges to the graph.
	 * 
	 * @param ain
	 * @return if this instruction marks the end of the block. Ex: a return instruction.
	 */
	public abstract boolean handleInsn(AbstractInsnNode ain, Block block);
}
