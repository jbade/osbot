package com.iinc.osupdater.deob.techniques.cfg.graph;

import java.util.ArrayList;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.deob.techniques.cfg.Block;
import com.iinc.osupdater.deob.techniques.cfg.CFGUtil;


public class JumpInsnHandler extends InsnHandler {

	public JumpInsnHandler(ArrayList<Block> blocks, MethodNode mn, DirectedPseudograph<Block, DefaultEdge> graph) {
		super(blocks, mn, graph);
	}

	@Override
	public boolean handleInsn(AbstractInsnNode ain, Block block) {
		// add edge if a jump
		if (ain instanceof JumpInsnNode) {
			JumpInsnNode jump = (JumpInsnNode) ain;
			Block to = CFGUtil.findBlockWithLabel(jump.label, blocks);
			if (to != null) {
				graph.addEdge(block, to);
			}

			// if an unconditional jump then this marks the end of the block
			if (jump.getOpcode() == Opcodes.GOTO) {
				return true;
			}
		}

		return false;
	}
}
