package com.iinc.osupdater.deob.techniques.cfg.graph;

import java.util.ArrayList;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TryCatchBlockNode;

import com.iinc.osupdater.deob.techniques.cfg.Block;
import com.iinc.osupdater.deob.techniques.cfg.CFGUtil;


public class AthrowInsnHandler extends InsnHandler {

	public AthrowInsnHandler(ArrayList<Block> blocks, MethodNode mn, DirectedPseudograph<Block, DefaultEdge> graph) {
		super(blocks, mn, graph);
	}

	@Override
	public boolean handleInsn(AbstractInsnNode ain, Block block) {
		// if athrow then add an edge to surrounding try-catch handler
		if (ain.getOpcode() == Opcodes.ATHROW) {
			TryCatchBlockNode tcbn = CFGUtil.getTryCatchBlockNode(mn, block);

			// tcbn == null occurs when method throws exception to caller
			if (tcbn != null) {
				Block handler = CFGUtil.findBlockWithLabel(tcbn.end, blocks);
				graph.addEdge(block, handler);
			}
			return true; // ATHROW instructions mark the end of the block
		}

		return false;
	}

}
