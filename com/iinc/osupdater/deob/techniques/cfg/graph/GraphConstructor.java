package com.iinc.osupdater.deob.techniques.cfg.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.deob.techniques.cfg.Block;


public class GraphConstructor {

	/**
	 * Generates the InsnHandlers for handling a method.
	 * 
	 * @param blocks
	 * @param mn
	 * @param graph
	 * @return
	 */
	private List<InsnHandler> generateInsnHandlers(ArrayList<Block> blocks, MethodNode mn, DirectedPseudograph<Block, DefaultEdge> graph) {
		LinkedList<InsnHandler> handlers = new LinkedList<InsnHandler>();
		handlers.add(new JumpInsnHandler(blocks, mn, graph));
		handlers.add(new AthrowInsnHandler(blocks, mn, graph));
		handlers.add(new ReturnInsnHandler(blocks, mn, graph));
		handlers.add(new TableSwitchHandler(blocks, mn, graph));
		return handlers;
	}

	public DirectedPseudograph<Block, DefaultEdge> constructGraph(MethodNode mn, ArrayList<Block> blocks) {
		DirectedPseudograph<Block, DefaultEdge> graph = new DirectedPseudograph<Block, DefaultEdge>(DefaultEdge.class);
		List<InsnHandler> handlers = generateInsnHandlers(blocks, mn, graph);

		// add all blocks to graph
		for (Block block : blocks) {
			graph.addVertex(block);
		}

		for (int i = 0; i < blocks.size(); i++) {
			Block block = blocks.get(i);
			ArrayList<AbstractInsnNode> blockInsns = blocks.get(i).getInstructions();

			for (int j = 0; j < blockInsns.size(); j++) {
				AbstractInsnNode ain = blockInsns.get(j);

				// handle instruction
				boolean endOfBlock = false;
				for (InsnHandler handler : handlers) {
					if (handler.handleInsn(ain, block)) {
						endOfBlock = true;
						break;
					}
				}
				
				if (endOfBlock) {
					// remove all dead instructions after an instruction marking the end of a block
					while (blockInsns.size() > j + 1) {
						blockInsns.remove(j + 1);
					}
					break; // finished handling this block
				}

				// add edge to flow down if last block
				if (j == blockInsns.size() - 1) { // last instruction
					if (i < blocks.size() - 1) { // not the last block
						graph.addEdge(block, blocks.get(i + 1));
					}
				}
			}
		}

		return graph;
	}
}
