package com.iinc.osupdater.deob.techniques.cfg.graph;

import java.util.ArrayList;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LookupSwitchInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TableSwitchInsnNode;

import com.iinc.osupdater.deob.techniques.cfg.Block;
import com.iinc.osupdater.deob.techniques.cfg.CFGUtil;

public class TableSwitchHandler extends InsnHandler {

	public TableSwitchHandler(ArrayList<Block> blocks, MethodNode mn, DirectedPseudograph<Block, DefaultEdge> graph) {
		super(blocks, mn, graph);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean handleInsn(AbstractInsnNode ain, Block block) {
		if (ain instanceof TableSwitchInsnNode) {
			TableSwitchInsnNode tsin = (TableSwitchInsnNode) ain;
			for (LabelNode label : (Iterable<LabelNode>) tsin.labels) {
				Block to = CFGUtil.findBlockWithLabel(label, blocks);
				if (to != null) {
					graph.addEdge(block, to);
				}
			}

			Block to = CFGUtil.findBlockWithLabel(tsin.dflt, blocks);
			if (to != null) {
				graph.addEdge(block, to);
			}

		} else if (ain instanceof LookupSwitchInsnNode) {
			LookupSwitchInsnNode lsin = (LookupSwitchInsnNode) ain;

			for (LabelNode label : (Iterable<LabelNode>) lsin.labels) {
				Block to = CFGUtil.findBlockWithLabel(label, blocks);
				if (to != null) {
					graph.addEdge(block, to);
				}
			}

			Block to = CFGUtil.findBlockWithLabel(lsin.dflt, blocks);
			if (to != null) {
				graph.addEdge(block, to);
			}
		}
		return false;
	}

}
