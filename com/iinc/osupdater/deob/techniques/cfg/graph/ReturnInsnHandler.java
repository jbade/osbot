package com.iinc.osupdater.deob.techniques.cfg.graph;

import java.util.ArrayList;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.deob.techniques.cfg.Block;


public class ReturnInsnHandler extends InsnHandler {

	public ReturnInsnHandler(ArrayList<Block> blocks, MethodNode mn, DirectedPseudograph<Block, DefaultEdge> graph) {
		super(blocks, mn, graph);
	}

	@Override
	public boolean handleInsn(AbstractInsnNode ain, Block block) {
		// doesn't need to add any edges. just mark end of block
		switch (ain.getOpcode()) {
		case Opcodes.RETURN:
		case Opcodes.DRETURN:
		case Opcodes.FRETURN:
		case Opcodes.IRETURN:
		case Opcodes.LRETURN:
			return true;
		}

		return false;
	}

}
