package com.iinc.osupdater.deob.techniques.cfg;

import java.util.ArrayList;
import java.util.Set;

import org.jgrapht.graph.DirectedPseudograph;
import org.jgrapht.graph.DefaultEdge;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;

public class BlockCombiner {

	/**
	 * Attempts to combine blocks together.
	 * 
	 * @param start
	 *            The starting block of the method
	 * @param blocks
	 * @param graph
	 */
	public void combineBlocks(Block start, ArrayList<Block> blocks, DirectedPseudograph<Block, DefaultEdge> graph) {
		for (int i = 0; i < blocks.size(); i++) {
			Block block = blocks.get(i);

			// don't remove blocks that are part of a try-catch
			if (!block.getTryCatchState().isNone()) {
				continue;
			}

			// don't remove start block
			if (block.equals(start)) {
				continue;
			}

			boolean failed = false;

			Set<DefaultEdge> edges = graph.edgesOf(block);
			DefaultEdge edge = null;
			for (DefaultEdge e : edges) {
				if (graph.getEdgeTarget(e).equals(block)) {
					if (edge != null) { // block must only have 1 edge pointing to it
						failed = true;
						break;
					}
					edge = e;
				}
			}

			if (edge == null || failed) { // 0 edges or too many edges
				continue;
			}

			Block source = graph.getEdgeSource(edge);

			// don't try to combine a block to itself
			if (source.equals(block)) {
				continue;
			}

			for (int j = 0; j < source.getInstructions().size(); j++) {
				AbstractInsnNode ain = source.getInstructions().get(j);

				// last instruction must be goto to block
				if (j == source.getInstructions().size() - 1) {
					if (ain.getOpcode() == Opcodes.GOTO) {
						JumpInsnNode jump = (JumpInsnNode) ain;
						if (jump.label.equals(block.getLabel())) {
							break; // success
						}
					}
					failed = true;
				}

				// jumps must not point to block except when last instruction
				// which is handled above
				if (ain instanceof JumpInsnNode) {
					JumpInsnNode jump = (JumpInsnNode) ain;
					if (jump.label.equals(block.getLabel())) {
						failed = true;
					}

				}
			}

			if (failed) {
				continue;
			}

			// we can combine blocks
			if (CFG.print) {
				System.out.println("Can combine block: " + block);
				System.out.println("With " + source);
			}

			// remove last GOTO instruction from source
			source.getInstructions().remove(source.getInstructions().size() - 1);

			// add instructions from block to source
			for (int j = 0; j < block.getInstructions().size(); j++) {
				// System.out.println(j);
				if (j == 0) { // ignore first instruction (LabelNode)
					continue;
				}
				source.addInsn(block.getInstructions().get(j));
			}

			// update graph
			graph.removeEdge(edge); // edge between source and block

			// move all edges from block to be from source
			edges = graph.edgesOf(block);
			for (DefaultEdge e : edges) {
				if (graph.getEdgeSource(e).equals(block)) {
					graph.addEdge(source, graph.getEdgeTarget(e));
					graph.removeEdge(e);
				}
			}

			graph.removeVertex(block);

			// remove block and adjust i
			blocks.remove(block);
			i--;
		}
	}
}
