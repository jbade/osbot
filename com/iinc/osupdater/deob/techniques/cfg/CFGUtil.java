package com.iinc.osupdater.deob.techniques.cfg;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jgrapht.DirectedGraph;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.graph.DefaultEdge;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TryCatchBlockNode;

public class CFGUtil {
	
	/**
	 * Gets the TryCatchBlockNode that the block in contained in. Null if not in a TryCatch. Used for handling ATHROW and RETURN.
	 * 
	 * @param mn
	 * @param block
	 * @return TryCatchBlockNode from mn.tryCatchBlocks that the block in contained in otherwise null
	 */
	@SuppressWarnings("unchecked")
	public static TryCatchBlockNode getTryCatchBlockNode(MethodNode mn, Block block) {
		if (mn == null || mn.tryCatchBlocks.size() == 0 || block == null || block.getLabel() == null) {
			return null;
		}

		for (TryCatchBlockNode tcbn : (List<TryCatchBlockNode>) mn.tryCatchBlocks) { // innermost-to-outermost
			boolean started = false;

			for (AbstractInsnNode ain : mn.instructions.toArray()) {
				if (ain instanceof LabelNode) {
					LabelNode label = (LabelNode) ain;

					// found start of tcbn
					if (!started && tcbn.start.equals(label)) {
						started = true;
					}

					// found end of tcbn
					if (started && tcbn.end.equals(label)) {
						break;
					}

					// found tcbn containing label
					if (started && label.equals(block.getLabel())) {
						return tcbn;
					}
				}
			}

		}

		return null;
	}

	/**
	 * Finds the block with the corresponding label
	 * 
	 * @param label
	 *            The label of the block to search for
	 * @param blocks
	 *            list of blocks
	 * @return Block or null if no match
	 */
	public static Block findBlockWithLabel(LabelNode label, ArrayList<Block> blocks) {
		if (label == null || blocks == null) {
			return null;
		}

		for (Block block : blocks) {
			if (block.getLabel() != null && block.getLabel().equals(label)) {
				return block;
			}
		}
		return null;
	}

	/**
	 * Saves graph to a .dot file
	 * 
	 * @param graph
	 * @param fileLocation
	 */
	public static <T> void saveGraphToFile(DirectedGraph<T, DefaultEdge> graph, String fileLocation) {
		IntegerNameProvider<T> p1 = new IntegerNameProvider<T>();
		BlockNameProvider p2 = new BlockNameProvider();

		DOTExporter<T, DefaultEdge> export = new DOTExporter<T, DefaultEdge>(p1, p2, null, null, null);
		try {
			export.export(new FileWriter(fileLocation), graph);
		} catch (IOException e) {
		}
	}
}
