package com.iinc.osupdater.deob.techniques.cfg;

import java.util.ArrayList;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.LabelNode;

import com.iinc.osupdater.util.InsnPrinter;


public class Block {
	private ArrayList<AbstractInsnNode> instructions = new ArrayList<AbstractInsnNode>();
	private LabelNode label;
	private BlockTryCatchState tryCatchState = new BlockTryCatchState();
	
	public Block() {
	}

	public void addInsn(AbstractInsnNode ain) {
		instructions.add(ain);
	}

	public void setLabel(AbstractInsnNode ln) {
		this.label = (LabelNode) ln;
		addInsn(ln);
	}

	public LabelNode getLabel() {
		return label;
	}

	public ArrayList<AbstractInsnNode> getInstructions() {
		return instructions;
	}
	
	public BlockTryCatchState getTryCatchState(){
		return tryCatchState;
	}



	public String getGraphLabelEscaped() {
		return toString().replaceAll("(?=[]\\[+&|!(){}^\"~*?:\\\\-])", "\\\\");
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(tryCatchState.toString() + "\n");
		
		for (AbstractInsnNode ain : instructions) {
			sb.append(InsnPrinter.prettyprint(ain));
		}

		return sb.toString();
	}
}
