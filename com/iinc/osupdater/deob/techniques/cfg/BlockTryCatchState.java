package com.iinc.osupdater.deob.techniques.cfg;

public class BlockTryCatchState {
	private boolean start;
	private boolean end;
	private boolean handler;

	public BlockTryCatchState() {
		start = false;
		end = false;
		handler = false;
	}

	public boolean isStart() {
		return start;
	}


	public void setStart(boolean start) {
		this.start = start;
	}


	public boolean isEnd() {
		return end;
	}


	public void setEnd(boolean end) {
		this.end = end;
	}


	public boolean isHandler() {
		return handler;
	}


	public void setHandler(boolean handler) {
		this.handler = handler;
	}
	
	public boolean isNone(){
		return !handler && !end && !start;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("TryCatchState: ");
		if (start){
			sb.append("start, ");
		}
		
		if (end){
			sb.append("end, ");
		}
		
		if (handler){
			sb.append("handler, ");
		}
		return sb.toString();
	}
}
