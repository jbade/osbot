package com.iinc.osupdater.deob.techniques.cfg;

import org.jgrapht.ext.VertexNameProvider;

public class BlockNameProvider<T> implements VertexNameProvider<Block> {

	@Override
	public String getVertexName(Block block) {
		return block.getGraphLabelEscaped();
	}

}
