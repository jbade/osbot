package com.iinc.osupdater.deob.techniques.cfg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TryCatchBlockNode;

import com.iinc.osupdater.deob.techniques.DeobfuscationTechnique;
import com.iinc.osupdater.deob.techniques.cfg.graph.GraphConstructor;
import com.iinc.osupdater.util.InsnPrinter;
import com.iinc.osupdater.util.JarHandler;

public class CFG implements DeobfuscationTechnique {
	public static boolean print = false;
	private static boolean saveGraph = true;
	private GraphConstructor graphConstructor = new GraphConstructor();
	private BlockCombiner blockCombiner = new BlockCombiner();

	public static void main(String args[]) {
		saveGraph = true;
		HashMap<String, ClassNode> classes = JarHandler.loadGamepack(117, true);

		for (ClassNode cn : classes.values()){
			if (cn.name.equals("dm")){
				for (MethodNode mn : (List<MethodNode>) cn.methods){
					if (mn.name.equals("b")){
						CFG cfg = new CFG();
						cfg.fixMethod(mn);
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean run(HashMap<String, ClassNode> classes, boolean print) {
		classes.values().forEach((cn) -> {
			for (MethodNode mn : (List<MethodNode>) cn.methods) {
				// if (cn.name.equals("client") && mn.name.equals("init") && mn.desc.equals("()V"))
				fixMethod(mn);
			}
		});

		System.out.println("Combined blocks");
		return true;
	}

	public void fixMethod(MethodNode mn) {
		// construct blocks
		ArrayList<Block> blocks = new ArrayList<Block>();
		Block start = constructBlocks(mn, blocks);

		initializeBlockTryCatchStates(blocks, mn);
		removeBlockFallThrough(blocks);

		DirectedPseudograph<Block, DefaultEdge> graph = graphConstructor.constructGraph(mn, blocks);

		if (CFG.saveGraph) {
			CFGUtil.saveGraphToFile(graph, "graphs/before.dot");
		}

		blockCombiner.combineBlocks(start, blocks, graph);

		if (CFG.saveGraph) {
			CFGUtil.saveGraphToFile(graph, "graphs/after.dot");
		}

		clearMethodNodeInstructions(mn);
		rebuildMethod(mn, blocks);
	}

	/**
	 * Adds a GOTO instruction to the end of every block that falls through to the block below it.
	 * 
	 * @param blocks
	 */
	private void removeBlockFallThrough(ArrayList<Block> blocks) {
		// add a GOTO to the end of this block pointing to the next block. This allows us to not have to deal with flow down
		// when combining blocks
		for (int i = 0; i < blocks.size() - 1; i++) { // don't iterate over last block
			Block block = blocks.get(i);
			AbstractInsnNode lastInsn = block.getInstructions().get(block.getInstructions().size() - 1);

			switch (lastInsn.getOpcode()) {
			case Opcodes.RETURN:
			case Opcodes.DRETURN:
			case Opcodes.FRETURN:
			case Opcodes.IRETURN:
			case Opcodes.LRETURN:
			case Opcodes.GOTO:
			case Opcodes.ATHROW:
				break;
			default:
				AbstractInsnNode nextBlockFirstInsn = blocks.get(i + 1).getInstructions().get(0);
				if (nextBlockFirstInsn instanceof LabelNode) {
					JumpInsnNode jump = new JumpInsnNode(Opcodes.GOTO, (LabelNode) nextBlockFirstInsn);
					block.getInstructions().add(jump);
				}
				break;
			}
		}
	}

	/**
	 * Constructs Blocks representing the instructions of the provided MethodNode. Blocks are made from labels and the following
	 * instructions.
	 * 
	 * @param mn
	 * @param blocks
	 * @return The starting Block. It's instructions may not start with LabelNode
	 */
	private Block constructBlocks(MethodNode mn, ArrayList<Block> blocks) {
		if (CFG.print) {
			System.out.println("Before: " + mn.name + ":" + mn.desc);
		}

		AbstractInsnNode[] insns = mn.instructions.toArray();

		Block block = new Block();
		Block start = block;
		blocks.add(block);

		// construct blocks
		for (int i = 0; i < insns.length; i++) {
			if (CFG.print) {
				System.out.print(InsnPrinter.prettyprint(insns[i]));
			}

			if (insns[i] instanceof LabelNode) {
				if (block.getInstructions().size() != 0) { // prevents empty start blocks
					block = new Block();
					blocks.add(block);
				}
				block.setLabel(insns[i]);
			} else {
				block.addInsn(insns[i]);
			}
		}

		if (print) {
			System.out.println("Finished Before");
		}

		return start;
	}

	/**
	 * Alternative to methodNode.instructions.clear() that updates the individual nodes correctly
	 * 
	 * @param mn
	 * @param oldInsns
	 */
	private void clearMethodNodeInstructions(MethodNode mn) {
		// clear the list. we cannot use .clear() because it does not update the
		// individual nodes. see: http://stackoverflow.com/q/36438214/3552325
		AbstractInsnNode[] insns = mn.instructions.toArray();
		for (int i = 0; i < insns.length; i++) {
			mn.instructions.remove(insns[i]);
		}
	}

	/**
	 * Rebuilds the MethodNode's instructions from the provided list of Blocks
	 * 
	 * @param mn
	 * @param blocks
	 * 
	 */
	private void rebuildMethod(MethodNode mn, List<Block> blocks) {
		// rebuild the instructions from the graph
		if (CFG.print) {
			System.out.println("After: " + mn.name + ":" + mn.desc);
		}

		for (Block b : blocks) {
			for (AbstractInsnNode ain : b.getInstructions()) {
				mn.instructions.add(ain);
				if (CFG.print) {
					System.out.print(InsnPrinter.prettyprint(ain));
				}
			}
		}
	}

	/**
	 * Updates all blocks with the correct TryCatchState
	 * 
	 * @param blocks
	 * @param mn
	 */
	@SuppressWarnings("unchecked")
	private void initializeBlockTryCatchStates(ArrayList<Block> blocks, MethodNode mn) {
		for (TryCatchBlockNode tcbn : (List<TryCatchBlockNode>) mn.tryCatchBlocks) {
			Block start = CFGUtil.findBlockWithLabel(tcbn.start, blocks);
			if (start != null) {
				start.getTryCatchState().setStart(true);
			}

			Block end = CFGUtil.findBlockWithLabel(tcbn.end, blocks);
			if (end != null) {
				end.getTryCatchState().setEnd(true);
			}

			Block handler = CFGUtil.findBlockWithLabel(tcbn.handler, blocks);
			if (handler != null) {
				handler.getTryCatchState().setHandler(true);
			}
		}
	}

}
