package com.iinc.osupdater.deob.techniques;

import java.util.HashMap;

import org.objectweb.asm.tree.ClassNode;

public interface DeobfuscationTechnique {
	/**
	 * 
	 * @param classes
	 * @param print
	 * @return true: no more work to be done </br>
	 *         false: potentially more work to be done
	 */
	public boolean run(HashMap<String, ClassNode> classes, boolean print);
}
