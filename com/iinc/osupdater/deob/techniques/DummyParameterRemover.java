package com.iinc.osupdater.deob.techniques;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;

import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

/**
 * Removes method instructions that check if dummy parameters are correct. Alter method description but does not change function calls.
 */
public class DummyParameterRemover implements DeobfuscationTechnique {
	private final Pattern pattern1;
	private final Pattern pattern2;

	// TODO do abstract methods?
	public DummyParameterRemover() {
		// build patterns
		List<PatternElement> list = new LinkedList<PatternElement>();
		list.add(PatternElement.b().opcodes(Opcodes.ILOAD).capture().b());
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).capture().b());
		list.add(PatternElement.b().opcodes(OpcodeGroups.IF_I).capture().b());
		list.add(PatternElement.b().opcodes(Opcodes.RETURN).b());
		pattern1 = new Pattern(list);

		list.remove(3);
		list.add(PatternElement.b().opcodes(Opcodes.NEW).b());
		list.add(PatternElement.b().opcodes(Opcodes.DUP).b());
		list.add(PatternElement.b().opcodes(Opcodes.INVOKESPECIAL).b());
		list.add(PatternElement.b().opcodes(Opcodes.ATHROW).b());
		pattern2 = new Pattern(list);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean run(HashMap<String, ClassNode> classes, boolean print) {
		int paramCount = 0; // counts how many dummy params we identified
		int checkCount = 0; // counts how many verification attempts we removed

		for (ClassNode cn : classes.values()) {
			for (MethodNode mn : (List<MethodNode>) cn.methods) {
				int result[] = processMethod(cn, mn, classes.values(), print);
				paramCount += result[0];
				checkCount += result[1];
			}
		}

		if (print)
			System.out.println("Removed dummy params (params " + paramCount + ")(checks " + checkCount + ")");

		return true;
	}

	/**
	 * Structure of instructions: if (dummy <>=! value){ return / throw new IllegalStateException(); }
	 * 
	 * iload1 ldc 1751667447 (java.lang.Integer) if_icmpne L6 new java/lang/IllegalStateException dup invokespecial
	 * java/lang/IllegalStateException <init>(()V); athrow
	 * 
	 * iload1 ldc 1494722858 (java.lang.Integer) if_icmpgt L14 return
	 * 
	 * 
	 * iload3 iconst_0 if_icmpgt L11 new java/lang/IllegalStateException dup invokespecial java/lang/IllegalStateException <init>(()V);
	 * athrow
	 * 
	 * iload3 bipush 15 if_icmpeq L6 new java/lang/IllegalStateException dup invokespecial java/lang/IllegalStateException <init>(()V);
	 * athrow
	 * 
	 */

	/**
	 * Processes the method and removes all dummy parameter checking. Replaces check with 'goto' to the correct label.
	 * 
	 * @param mn
	 *            The method to process.
	 */
	public int[] processMethod(ClassNode cn, MethodNode mn, Collection<ClassNode> classes, boolean print) {
		Type[] params = Type.getArgumentTypes(mn.desc);
		int firstParam = AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC) ? 0 : 1;
		int lastParam = firstParam;

		for (int i = 0; i < params.length - 1; i++) {
			lastParam += params[i].getSize();
		}

		int paramCount = 0; // counts how many dummy params we identified
		int checkCount = 0; // counts how many verification attempts we removed

		// The potential dummy parameter candidates and how many matches appear in the method
		HashMap<DummyParam, Integer> candidates = new HashMap<DummyParam, Integer>();

		// match patterns
		List<PatternMatchResult> results1 = pattern1.match(mn);
		List<PatternMatchResult> results2 = pattern2.match(mn);

		// construct candidates
		LinkedList<PatternMatchResult> list = new LinkedList<>();
		list.addAll(results1);
		list.addAll(results2);
		for (PatternMatchResult match : list) {
			DummyParam param = constructDummyParam(mn, params, match);
			if (param != null) {
				if (candidates.containsKey(param)) {
					candidates.put(param, candidates.get(param) + 1);
				} else {
					candidates.put(param, 1);
				}
			}
		}

		// count the number of time each parameter is loaded
		HashMap<Integer, Integer> iloadCounts = new HashMap<Integer, Integer>();
		for (AbstractInsnNode ain : mn.instructions.toArray()) {
			if (ain instanceof VarInsnNode) {
				VarInsnNode iload = (VarInsnNode) ain;
				if (iload.var >= firstParam && iload.var <= lastParam) {
					if (iloadCounts.containsKey(iload.var)) {
						iloadCounts.put(iload.var, iloadCounts.get(iload.var) + 1);
					} else {
						iloadCounts.put(iload.var, 1);
					}
				}
			}
		}

		// check to make sure that potential dummy parameters are never used elsewhere besides in a dummy param check. This invalidates
		// params that are compared against different values because they have different DummyParam objects.
		Iterator<DummyParam> iterator = candidates.keySet().iterator();
		while (iterator.hasNext()) {
			DummyParam dp = iterator.next();
			if (iloadCounts.get(dp.var) != candidates.get(dp)) {
				iterator.remove();
			}
		}

		// check if last param is never used
		// must be primitive, must have try catch
		if (params.length > 0 && mn.tryCatchBlocks.size() > 0 && !params[params.length - 1].toString().startsWith("[")
				&& !params[params.length - 1].toString().startsWith("L")) {
			if (!iloadCounts.containsKey(lastParam)) {
				DummyParam param = new DummyParam(params.length - 1, lastParam, null);
				candidates.put(param, 0);
			}
		}

		// if (print) {
		// System.out.println("final candidates: " + candidates);
		// }

		// there should only be 1
		if (candidates.size() > 1) {
			System.err.println("More than 1 dummy param indentified in " + cn.name + "." + mn.name + mn.desc);
			return new int[] { 0, 0 };
		}

		paramCount = candidates.size();

		// remove all checks from this method
		for (PatternMatchResult match : results1) {
			VarInsnNode iload = (VarInsnNode) match.getCaptures().get(0);
			for (DummyParam dp : candidates.keySet()) { // only remove the match if it was identified as a dummy param
				if (dp.var == iload.var) {
					JumpInsnNode jump = new JumpInsnNode(Opcodes.GOTO, ((JumpInsnNode) match.getCaptures().get(2)).label);
					mn.instructions.insertBefore(match.getStart(), jump);

					AbstractInsnNode curr = match.getStart();
					for (int j = 0; j < 4; j++) {
						AbstractInsnNode temp = curr;
						curr = curr.getNext();
						mn.instructions.remove(temp);
					}
					checkCount++;
					break;
				}
			}
		}
		for (PatternMatchResult match : results2) {
			VarInsnNode iload = (VarInsnNode) match.getCaptures().get(0);
			for (DummyParam dp : candidates.keySet()) { // only remove the match if it was identified as a dummy param
				if (dp.var == iload.var) {
					JumpInsnNode jump = new JumpInsnNode(Opcodes.GOTO, ((JumpInsnNode) match.getCaptures().get(2)).label);
					mn.instructions.insertBefore(match.getStart(), jump);

					AbstractInsnNode curr = match.getStart();
					for (int j = 0; j < 7; j++) {
						AbstractInsnNode temp = curr;
						curr = curr.getNext();
						mn.instructions.remove(temp);
					}
					checkCount++;
					break;
				}
			}
		}

		changeDesc(mn, params, candidates.keySet());

		// if (print) {
		// System.out.println("desc after: " + mn.desc);
		// }

		return new int[] { paramCount, checkCount };
	}

	/**
	 * Removes dummy params from the MethodNode's desc.
	 * 
	 * @param mn
	 * @param params
	 * @param dummyParams
	 */
	private void changeDesc(MethodNode mn, Type[] params, Set<DummyParam> dummyParams) {
		ArrayList<Type> paramsList = new ArrayList<Type>(Arrays.asList(params));

		// set all params to remove to null
		for (DummyParam dp : dummyParams) {
			paramsList.set(dp.index, null);
		}

		// remove null objects from list
		Iterator<Type> iterator = paramsList.iterator();
		while (iterator.hasNext()) {
			if (iterator.next() == null) {
				iterator.remove();
			}
		}

		// generate desc
		params = new Type[paramsList.size()];
		mn.desc = Type.getMethodDescriptor(Type.getReturnType(mn.desc), paramsList.toArray(params));
	}

	/**
	 * Constructs a DummyParam for the specified match.
	 * 
	 * @param match
	 * @param firstParam
	 * @param lastParam
	 * @return DummyParam or null
	 */
	private DummyParam constructDummyParam(MethodNode mn, Type[] params, PatternMatchResult match) {
		VarInsnNode iload = (VarInsnNode) match.getCaptures().get(0);

		// dummy params only appear as the last param but this does not check that
		int localVariableIndex = 0;
		if (!AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC))
			localVariableIndex = 1; // this
		int index = -1;
		for (int i = 0; i < params.length; i++) {
			if (localVariableIndex == iload.var) {
				index = i;
				break;
			}
			localVariableIndex += params[i].getSize();
		}

		// not a param
		if (index == -1) {
			return null;
		}

		AbstractInsnNode ain = match.getCaptures().get(1);
		Object value = null;
		if (ain instanceof IntInsnNode) {
			value = ((IntInsnNode) ain).operand;
		} else if (ain instanceof LdcInsnNode) {
			value = ((LdcInsnNode) ain).cst;
		} else if (ain instanceof InsnNode) {
			switch (ain.getOpcode()) {
			case Opcodes.ICONST_0:
				value = 0;
				break;
			case Opcodes.ICONST_1:
				value = 1;
				break;
			case Opcodes.ICONST_2:
				value = 2;
				break;
			case Opcodes.ICONST_3:
				value = 3;
				break;
			case Opcodes.ICONST_4:
				value = 4;
				break;
			case Opcodes.ICONST_5:
				value = 5;
				break;
			case Opcodes.ICONST_M1:
				value = -1;
				break;
			case Opcodes.DCONST_0:
				value = 0.0;
				break;
			case Opcodes.DCONST_1:
				value = 1.0;
				break;
			case Opcodes.FCONST_0:
				value = 0.0f;
				break;
			case Opcodes.FCONST_1:
				value = 1.0f;
				break;
			case Opcodes.FCONST_2:
				value = 2.0f;
				break;
			case Opcodes.LCONST_0:
				value = 0L;
				break;
			case Opcodes.LCONST_1:
				value = 1L;
				break;
			}
		}

		return new DummyParam(index, iload.var, value);
	}

	private class DummyParam {
		int index; // index in params array
		int var;
		Object value; // value param is being compared against

		public DummyParam(int index, int var, Object value) {
			this.index = index;
			this.var = var;
			this.value = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + index;
			result = prime * result + var;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null)
				return false;

			DummyParam other = (DummyParam) obj;
			return index == other.index && var == other.var && (value == null && other.value == null || value.equals(other.value));
		}

		@Override
		public String toString() {
			return "DummyParam [index=" + index + ", var=" + var + ", value=" + value + "]";
		}
	}
}
