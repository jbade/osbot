package com.iinc.osupdater.deob.techniques;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

public class RedundantFieldRemover implements DeobfuscationTechnique {
	private HashMap<String, ClassNode> classes;
	private HashSet<FieldWrapper> used; // fields that have been processed and are used in the gamepack

	@SuppressWarnings("unchecked")
	public boolean run(HashMap<String, ClassNode> classes, boolean print) {
		this.classes = classes;
		used = new HashSet<FieldWrapper>();

		for (ClassNode cn : classes.values()) {
			for (MethodNode mn : (Iterable<MethodNode>) cn.methods) {
				// check all method instructions for use of fields
				for (AbstractInsnNode ain : mn.instructions.toArray()) {
					if (ain instanceof FieldInsnNode) {
						FieldInsnNode mns = (FieldInsnNode) ain;
						// find the field and add it to used
						if (hasField(get(mns.owner), mns.name, mns.desc)) {
							used.add(new FieldWrapper(mns.owner, mns.name, mns.desc));
						} else {
							ClassNode cn2 = get(mns.owner);
							String classSuper = cn2.superName;
							while (classSuper != null) { // classSuper will never be null
								ClassNode cn3 = get(classSuper);
								if (hasField(cn3, mns.name, mns.desc)) {
									used.add(new FieldWrapper(cn3.name, mns.name, mns.desc));
									break;
								}
								classSuper = cn3.superName;
							}
						}

					}
				}
			}
		}

		// count all fields
		int all = 0;
		for (ClassNode cn : classes.values()) {
			all += cn.fields.size();
		}

		// remove unused fields
		int removed = 0;
		for (ClassNode cn : classes.values()) {
			Iterator<FieldNode> i = cn.fields.iterator();
			while (i.hasNext()) {
				FieldNode fn = i.next();
				if (!used.contains(new FieldWrapper(cn.name, fn.name, fn.desc))) {
					i.remove();
					removed++;
				}
			}
		}

		// count all fields again
		int newAll = 0;
		for (ClassNode cn : classes.values()) {
			newAll += cn.fields.size();
		}
		if (print)
			System.out.println("Redundant Field Removal: " + newAll + "/" + all + " (removed " + removed + ")");
		
		return removed == 0;
	}

	/**
	 * Gets a ClassNode of the provided class name from HashMap<String, ClassNode> classes or java
	 * 
	 * @param name
	 *            class name
	 * @return ClassNode representing the class with the specified name
	 */
	private ClassNode get(String name) {
		if (name == null)
			return null;

		if (classes.containsKey(name)) {
			return classes.get(name);
		}

		ClassNode cn = new ClassNode();
		try {
			ClassReader cr = new ClassReader(name);
			cr.accept(cn, 0);
		} catch (Exception e) {
			return null;
		}
		return cn;
	}

	/**
	 * Checks whether a ClassNode has a specific field
	 * 
	 * @param node
	 *            class to check for the field
	 * @param name
	 *            field's name
	 * @param desc
	 *            field's description
	 * @return true: the class has the field </br>
	 *         false: the class does not have the field
	 */
	@SuppressWarnings("unchecked")
	private static boolean hasField(ClassNode node, String name, String desc) {
		for (FieldNode fn : (List<FieldNode>) node.fields) {
			if (fn.name.equals(name) && fn.desc.equals(desc)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * class that represents a field
	 */
	class FieldWrapper {
		private String owner, name, desc;

		public FieldWrapper(String owner, String name, String desc) {
			this.owner = owner;
			this.name = name;
			this.desc = desc;
		}

		public String getOwner() {
			return owner;
		}

		public String getName() {
			return name;
		}

		public String getDesc() {
			return desc;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((desc == null) ? 0 : desc.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((owner == null) ? 0 : owner.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof FieldWrapper))
				return false;
			FieldWrapper other = (FieldWrapper) obj;
			return desc.equals(other.desc) && name.equals(other.name) && owner.equals(other.owner);
		}

		@Override
		public String toString() {
			return owner + "." + name + desc;
		}
	}
}
