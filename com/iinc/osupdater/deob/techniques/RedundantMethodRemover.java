package com.iinc.osupdater.deob.techniques;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.StringNameProvider;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

public class RedundantMethodRemover implements DeobfuscationTechnique {
	private HashMap<String, ClassNode> classes;
	private DirectedGraph<String, DefaultEdge> graph; // graph representing
														// class and interface
														// relations
	private HashSet<MethodWrapper> used; // methods that have been processed and
											// are used in the gamepack
	private Queue<MethodWrapper> toProcess; // methods that need to be processed

	@SuppressWarnings("unused")
	private void saveGraphToFile(String fileLocation) {
		IntegerNameProvider<String> p1 = new IntegerNameProvider<String>();
		StringNameProvider<String> p2 = new StringNameProvider<String>();

		DOTExporter<String, DefaultEdge> export = new DOTExporter<String, DefaultEdge>(p1, p2, null, null, null);
		try {
			export.export(new FileWriter(fileLocation), graph);
		} catch (IOException e) {
		}
	}

	/**
	 * Generates a directed graph representing the hierarchy of classes/interfaces. </br>
	 * Edge: source=parent, target=child </br>
	 */
	@SuppressWarnings("unchecked")
	private void generateGraph() {
		graph = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
		for (ClassNode cn : classes.values()) {
			graph.addVertex(cn.name);

			if (cn.superName.contains("java")) {
				// add all parent classes/interfaces
				ClassNode prevOwner = cn;
				ClassNode owner = get(cn.superName);
				while (owner != null) {
					graph.addVertex(owner.name);
					graph.addEdge(owner.name, prevOwner.name);

					for (String i : (List<String>) owner.interfaces) {
						graph.addVertex(i);
						graph.addEdge(i, owner.name);
						// there is no need to check for the parents of
						// interfaces because they are not used in this version
						// of runescape/java
					}
					prevOwner = owner;
					owner = get(owner.superName);
				}
			} else {
				// add just 1 parent
				graph.addVertex(cn.superName);
				graph.addEdge(cn.superName, cn.name);
			}

			// add interfaces
			for (String i : (List<String>) cn.interfaces) {
				graph.addVertex(i);
				graph.addEdge(i, cn.name);
				// there is no need to check for the parents of interfaces
				// because they are not used in this version of runescape/java
			}
		}
	}

	/**
	 * Gets all the children of a class/interface using the provided graph
	 * 
	 * @param graph
	 *            graph used to find children
	 * @param parent
	 *            class/interface name
	 * @return Set of class/interface names
	 */
	public Set<String> getChildren(DirectedGraph<String, DefaultEdge> graph, String parent) {
		Set<String> children = new HashSet<String>();
		if (graph.containsVertex(parent)) {
			for (DefaultEdge edge : graph.edgesOf(parent)) {
				if (!graph.getEdgeTarget(edge).equals(parent)) {
					children.add(graph.getEdgeTarget(edge));
				}
			}
		}
		return children;
	}

	/**
	 * Gets the parent class/interfaces of a class/interface using the provided graph
	 * 
	 * @param graph
	 *            graph used to find parents
	 * @param child
	 *            class/interface name
	 * @return Set of class/interface names
	 */
	public Set<String> getParents(DirectedGraph<String, DefaultEdge> graph, String child) {
		Set<String> parents = new HashSet<String>();
		if (graph.containsVertex(child)) {
			for (DefaultEdge edge : graph.edgesOf(child)) {
				if (graph.getEdgeTarget(edge).equals(child)) {
					parents.add(graph.getEdgeSource(edge));
				}
			}
		}
		return parents;
	}

	@SuppressWarnings("unchecked")
	public boolean run(HashMap<String, ClassNode> classes, boolean print) {
		this.classes = classes;
		used = new HashSet<MethodWrapper>();
		toProcess = new LinkedList<MethodWrapper>();

		generateGraph();

		// implemented/overridden interface methods
		for (ClassNode cn : classes.values()) {
			String classSuper = cn.name;
			while (!classSuper.equals("java/lang/Object")) {
				ClassNode cn2 = get(classSuper);
				for (String i : (List<String>) cn2.interfaces) {
					for (MethodNode mn : (List<MethodNode>) get(i).methods) {
						if (hasMethod(cn, mn.name, mn.desc)) {
							MethodWrapper rapper = new MethodWrapper(cn.name, mn.name, mn.desc);
							if (!toProcess.contains(rapper) && !used.contains(rapper)) {
								toProcess.add(rapper);
							}
						}
					}
				}

				classSuper = cn2.superName;
			}
		}

		// method called when applet is started (client.init)
		toProcess.add(new MethodWrapper("client", "init", "()V"));

		// when client object is created
		toProcess.add(new MethodWrapper("client", "<init>", "()V"));

		// process queue
		while (!toProcess.isEmpty()) {
			MethodWrapper mw = toProcess.poll(); // get a method
			used.add(mw);

			ClassNode cn = get(mw.getOwner());

			for (MethodNode mn : (List<MethodNode>) cn.methods) {
				// when the class is loaded
				if (mn.name.equals("<clinit>")) {
					MethodWrapper c = new MethodWrapper(cn.name, mn.name, mn.desc);
					if (!toProcess.contains(c) && !used.contains(c)) {
						toProcess.add(c);
					}
				}

				// find the method
				if (mn.name.equals(mw.getName()) && mn.desc.equals(mw.desc)) {
					// loop through instructions and add methods to toProcess
					for (AbstractInsnNode ain : mn.instructions.toArray()) {
						if (ain instanceof MethodInsnNode) {
							MethodInsnNode min = (MethodInsnNode) ain;

							// check owner/owner's parents until found
							ClassNode cn2 = get(min.owner);
							while (cn2 != null) {
								if (hasMethod(cn2, min.name, min.desc) && classes.containsKey(cn2.name)) {
									MethodWrapper rapper = new MethodWrapper(cn2.name, min.name, min.desc);
									if (!toProcess.contains(rapper) && !used.contains(rapper)) {
										toProcess.add(rapper);
									}
									break;
								}
								cn2 = get(cn2.superName);
							}

							// check all of owner's children and owner's
							// children's children... for method
							checkChildren(min.owner, min);

						} else if (ain instanceof FieldInsnNode) {
							// process <clinit> call when accessing static
							// fields of classes whose methods are never called
							FieldInsnNode mns = (FieldInsnNode) ain;
							MethodWrapper c = new MethodWrapper(mns.owner, "<clinit>", "()V");
							if (!toProcess.contains(c) && !used.contains(c)) {
								toProcess.add(c);
							}
						}
					}
				}
			}
		}

		// count all methods
		int all = 0;
		for (ClassNode cn : classes.values()) {
			all += cn.methods.size();
		}

		// remove unused methods
		int removed = 0;
		for (ClassNode cn : classes.values()) {
			Iterator<MethodNode> i = cn.methods.iterator();
			while (i.hasNext()) {
				MethodNode mn = (MethodNode) i.next();
				if (!used.contains(new MethodWrapper(cn.name, mn.name, mn.desc))) {
					i.remove();
					removed++;
				}
			}
		}

		// count all methods again
		int newAll = 0;
		for (ClassNode cn : classes.values()) {
			newAll += cn.methods.size();
		}

		if (print)
			System.out.println("Redundant Method Removal: " + newAll + "/" + all + " (removed " + removed + ")");

		return removed == 0;
	}

	/**
	 * Recursively checks child classes of classToCheck for methods matching min
	 * 
	 * @param classToCheck
	 *            class name to check children of
	 * @param min
	 *            method to check for
	 */
	public void checkChildren(String classToCheck, MethodInsnNode min) {
		for (String child : getChildren(graph, classToCheck)) {
			ClassNode owner = get(child);
			if (hasMethod(owner, min.name, min.desc) && classes.containsKey(owner.name)) {
				MethodWrapper rapper = new MethodWrapper(owner.name, min.name, min.desc);
				if (!toProcess.contains(rapper) && !used.contains(rapper)) {
					toProcess.add(rapper);
				}
			}

			checkChildren(child, min);
		}
	}

	/**
	 * Gets a ClassNode of the provided class name from HashMap<String, ClassNode> classes or java
	 * 
	 * @param name
	 *            class name
	 * @return ClassNode representing the class with the specified name
	 */
	private ClassNode get(String name) {
		if (name == null)
			return null;

		if (classes.containsKey(name)) {
			return classes.get(name);
		}

		ClassNode cn = new ClassNode();
		try {
			ClassReader cr = new ClassReader(name);
			cr.accept(cn, 0);
		} catch (Exception e) {
			return null;
		}
		return cn;
	}

	/**
	 * Checks whether a ClassNode has a specific method
	 * 
	 * @param node
	 *            class to check for the method
	 * @param name
	 *            method's name
	 * @param desc
	 *            method's description
	 * @return true: the class has the method </br>
	 *         false: the class does not have the method
	 */
	@SuppressWarnings("unchecked")
	private static boolean hasMethod(ClassNode node, String name, String desc) {
		for (MethodNode mn : (List<MethodNode>) node.methods) {
			if (mn.name.equals(name) && mn.desc.equals(desc)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * class that represents a method
	 */
	class MethodWrapper {
		private String owner, name, desc;

		public MethodWrapper(String owner, String name, String desc) {
			this.owner = owner;
			this.name = name;
			this.desc = desc;
		}

		public String getOwner() {
			return owner;
		}

		public String getName() {
			return name;
		}

		public String getDesc() {
			return desc;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((desc == null) ? 0 : desc.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((owner == null) ? 0 : owner.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof MethodWrapper))
				return false;
			MethodWrapper other = (MethodWrapper) obj;
			return desc.equals(other.desc) && name.equals(other.name) && owner.equals(other.owner);
		}

		@Override
		public String toString() {
			return owner + "." + name + desc;
		}
	}
}
