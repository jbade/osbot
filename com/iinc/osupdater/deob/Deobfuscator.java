package com.iinc.osupdater.deob;

import java.util.HashMap;
import java.util.LinkedList;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.deob.techniques.ArithmeticDeobfuscation;
import com.iinc.osupdater.deob.techniques.DeobfuscationTechnique;
import com.iinc.osupdater.deob.techniques.DummyParameterRemover;
import com.iinc.osupdater.deob.techniques.RedundantFieldRemover;
import com.iinc.osupdater.deob.techniques.RedundantMethodRemover;
import com.iinc.osupdater.deob.techniques.cfg.CFG;

public class Deobfuscator {
	public LinkedList<DeobfuscationTechnique> techniques;

	public Deobfuscator() {
		techniques = new LinkedList<DeobfuscationTechnique>();
		techniques.add(new RedundantMethodRemover());
		techniques.add(new CFG());
		techniques.add(new RedundantFieldRemover());
		techniques.add(new DummyParameterRemover());
		techniques.add(new ArithmeticDeobfuscation());
	}

	public void run(HashMap<String, ClassNode> classes, boolean print) {
		for (DeobfuscationTechnique technique : techniques) {
			long startTime = System.currentTimeMillis();
			while (!technique.run(classes, print))
				;
			if (print)
				System.out.println("(" + (System.currentTimeMillis() - startTime) + " ms)");
		}

		if (print)
			System.out.println();
	}
}
