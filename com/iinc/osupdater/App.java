package com.iinc.osupdater;

import java.io.IOException;
import java.util.HashMap;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.GamepackAnalyzer;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.log.LogHandler;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.deob.Deobfuscator;
import com.iinc.osupdater.util.JarHandler;

public class App {
	public static void main(String[] args) throws IOException, IdentificationException {
		if (false){
			HashMap<String, ClassNode> classes = JarHandler.update(true);
			Deobfuscator deob = new Deobfuscator();
			deob.run(classes, true);
			JarHandler.saveGamepack(classes, true);
		} else if (true){
			int rev = 154;
			HashMap<String, ClassNode> classes = JarHandler.loadGamepack(rev, true);
			//JarHandler.revision(classes.get("client"));
			GamepackAnalyzer a = new GamepackAnalyzer();
			Info info = a.run(classes, true);
			System.out.print(info);

			LogHandler.save(info, rev);
		} else {
			int rev = 133;
			HashMap<String, ClassNode> classes = JarHandler.loadGamepack(rev, false);
			Deobfuscator deob = new Deobfuscator();
			deob.run(classes, true);
			JarHandler.saveGamepack(classes, true);
		}
	}
}
