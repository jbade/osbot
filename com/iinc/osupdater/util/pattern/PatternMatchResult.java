package com.iinc.osupdater.util.pattern;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.tree.AbstractInsnNode;

import com.iinc.osupdater.util.InsnPrinter;

/**
 * Represents the result of Pattern.match
 */
public class PatternMatchResult {
	private AbstractInsnNode start; // the starting insn of a match
	private ArrayList<AbstractInsnNode> captures = new ArrayList<AbstractInsnNode>(); // the nodes that were captured

	/**
	 * Constructs a PatternMatchResult with the a null start and an empty list of captures.
	 */
	public PatternMatchResult() {
	}

	/**
	 * Constructs a PatternMatchResult with the specified start and an empty list of captures.
	 * 
	 * @param start
	 */
	public PatternMatchResult(AbstractInsnNode start) {
		this.start = start;
	}

	/**
	 * Constructs a PatternMatchResult with the specified start and the specified captures.
	 * 
	 * @param start
	 * @param captures
	 */
	public PatternMatchResult(AbstractInsnNode start, List<AbstractInsnNode> captures) {
		this.start = start;
		this.captures.addAll(captures);
	}

	/**
	 * @return The starting AbstractInsnNode of a match result.
	 */
	public AbstractInsnNode getStart() {
		return start;
	}

	/**
	 * @return A list of captured AbstractInsnNodes.
	 */
	public ArrayList<AbstractInsnNode> getCaptures() {
		return captures;
	}

	/**
	 * Sets the starting node of this MatchResult
	 * 
	 * @param start
	 */
	void setStart(AbstractInsnNode start) {
		this.start = start;
	}

	/**
	 * Adds the AbstractInsnNode to the list of captured insns.
	 * 
	 * @param capture
	 */
	void addCapture(AbstractInsnNode capture) {
		this.captures.add(capture);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("PatternMatchResult\n");
		sb.append("\tstart:   ");
		sb.append(InsnPrinter.prettyprint(start));
		sb.append("\tcaptures:");
		boolean first = true;
		for (AbstractInsnNode ain : captures){
			if (first){
				first = false;
			} else {
				sb.append("\t\t ");
			}
			
			sb.append(InsnPrinter.prettyprint(ain));
		}
		
		return sb.toString();
	}
}
