package com.iinc.osupdater.util.pattern;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodNode;

/**
 * Utility class for identifying bytecode patterns in MethodNodes. Allows for capturing individual instructions similar
 * to capture groups in regex.
 */
public class Pattern {
	private LinkedList<PatternElement> elements = new LinkedList<PatternElement>();

	/**
	 * Constructs a Pattern with the specified elements. GOTO and Label instructions are automatically followed and
	 * ignored when checking for matches. Specify in individual {@link PatternElement}s whether to capture and whether
	 * to follow jump instructions.
	 * 
	 * @param elements
	 */
	public Pattern(List<PatternElement> elements) {
		this.elements.addAll(elements);
	}

	/**
	 * Constructs a Pattern with the specified elements. GOTO and Label instructions are automatically followed and
	 * ignored when checking for matches. Specify in individual {@link PatternElement}s whether to capture and whether
	 * to follow jump instructions.
	 * 
	 * @param elements
	 */
	public Pattern(PatternElement... elements) {
		Collections.addAll(this.elements, elements);
	}
	
	/**
	 * Tests the provided MethodNode for matches.
	 * 
	 * @param mn
	 * @return A list of {@link PatternMatchResult}s.
	 */
	public List<PatternMatchResult> match(MethodNode mn) {
		LinkedList<PatternMatchResult> results = new LinkedList<PatternMatchResult>();

		for (AbstractInsnNode start : mn.instructions.toArray()) {
			// don't start with labels or gotos
			if (start instanceof LabelNode || start.getOpcode() == Opcodes.GOTO) {
				continue;
			}

			boolean failed = false;
			PatternMatchResult matchResult = new PatternMatchResult(start);

			AbstractInsnNode curr = start;
			for (PatternElement pe : elements) { // attempt to match all elements
				if (curr == null){
					failed = true;
					break;
				}
				
				// follow gotos and skip labels
				while (curr instanceof LabelNode || curr.getOpcode() == Opcodes.GOTO) {
					if (curr instanceof LabelNode) {
						curr = curr.getNext();
					}

					if (curr.getOpcode() == Opcodes.GOTO) {
						curr = ((JumpInsnNode) curr).label;
					}
				}

				// test for match
				if (pe.match(curr)) {
					// capture if necessary
					if (pe.shouldCapture()) {
						matchResult.addCapture(curr);
					}

					// handle jump
					if (pe.shouldJump()) {
						if (curr instanceof JumpInsnNode) {
							curr = ((JumpInsnNode) curr).label;
							continue;
						}
					}

					// continue to next insn
					curr = curr.getNext();
				} else {
					// doesn't match
					failed = true;
					break;
				}
			}

			if (!failed) {
				results.add(matchResult); // we found a match
			}
		}

		return results;
	}

	public static List<PatternElement> makeList() {
		return new LinkedList<PatternElement>();
	}
}
