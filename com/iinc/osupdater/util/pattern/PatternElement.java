package com.iinc.osupdater.util.pattern;

import java.util.LinkedList;

import org.objectweb.asm.tree.AbstractInsnNode;

public class PatternElement {
	private LinkedList<Integer> opcodes = new LinkedList<Integer>();
	private boolean jump = false;
	private boolean capture = false;

	/**
	 * Constructs a PatternElement with the specified flags and opcodes.
	 * 
	 * @param opcodes
	 */
	private PatternElement(int[] opcodes, boolean jump, boolean capture) {
		this.jump = jump;
		this.capture = capture;
		for (int op : opcodes) {
			this.opcodes.add(op);
		}
	}

	/**
	 * Shorter name for builder
	 * 
	 * @return
	 */
	public static PatternElementBuilder b() {
		return builder();
	}

	public static PatternElementBuilder builder() {
		return new PatternElementBuilder();
	}

	public static class PatternElementBuilder {
		private boolean jump = false;
		private boolean capture = false;
		private int[] opcodes;

		/**
		 * Sets the opcodes to the provided opcodes.
		 * 
		 * @param opcodes
		 * @return
		 */
		public PatternElementBuilder opcodes(int... opcodes) {
			this.opcodes = opcodes;
			return this;
		}

		/**
		 * Sets jump to true.
		 * 
		 * @return
		 */
		public PatternElementBuilder jump() {
			this.jump = true;
			return this;
		}

		/**
		 * Sets capture to true.
		 * 
		 * @return
		 */
		public PatternElementBuilder capture() {
			this.capture = true;
			return this;
		}

		public PatternElement build() {
			return new PatternElement(opcodes, jump, capture);
		}

		/**
		 * Shorter name for build
		 * 
		 * @return
		 */
		public PatternElement b() {
			return build();
		}
	}

	/**
	 * If the PatternFinder should follow a JumpInsn or if it should continue sequentially.
	 * 
	 * @return
	 */
	public boolean shouldJump() {
		return jump;
	}

	/**
	 * If this Element should be captured as a result of Pattern.match
	 * 
	 * @return
	 */
	public boolean shouldCapture() {
		return capture;
	}

	/**
	 * Check if this PatternElement matches the specified AbstractInsnNode. Checks for a match using opcodes. An empty
	 * list of opcodes matches anything.
	 * 
	 * @param ain
	 * @return
	 */
	public boolean match(AbstractInsnNode ain) {
		if (ain == null) {
			return false;
		}

		// empty list matches anything
		if (opcodes.size() == 0) {
			return true;
		}

		// check list
		for (Integer op : opcodes) {
			if (ain.getOpcode() == op) {
				return true;
			}
		}

		return false;
	}
}
