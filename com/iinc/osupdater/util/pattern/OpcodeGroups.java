package com.iinc.osupdater.util.pattern;

import org.objectweb.asm.Opcodes;

/**
 * Provides arrays of commonly grouped instructions that can be used when building {@link PatternElement}s.
 */
public class OpcodeGroups {
	// Instructions for comparing references and values.
	public final static int IF[] = { Opcodes.IF_ACMPEQ, Opcodes.IF_ACMPNE, Opcodes.IF_ICMPEQ, Opcodes.IF_ICMPGE, Opcodes.IF_ICMPGT,
			Opcodes.IF_ICMPLE, Opcodes.IF_ICMPLT, Opcodes.IF_ICMPNE };
	// Instructions for comparing values.
	public final static int IF_I[] = { Opcodes.IF_ICMPEQ, Opcodes.IF_ICMPGE, Opcodes.IF_ICMPGT, Opcodes.IF_ICMPLE, Opcodes.IF_ICMPLT,
			Opcodes.IF_ICMPNE };
	// Instructions for comparing references.
	public final static int IF_A[] = { Opcodes.IF_ACMPEQ, Opcodes.IF_ACMPNE };

	// Instructions for loading some constant onto the stack.
	public final static int PUSH_CONSTANT[] = { Opcodes.DCONST_0, Opcodes.DCONST_1, Opcodes.FCONST_0, Opcodes.FCONST_1, Opcodes.FCONST_2,
			Opcodes.LCONST_0, Opcodes.LCONST_1, Opcodes.LDC, Opcodes.BIPUSH, Opcodes.ICONST_0, Opcodes.ICONST_1, Opcodes.ICONST_2,
			Opcodes.ICONST_3, Opcodes.ICONST_4, Opcodes.ICONST_5, Opcodes.ICONST_M1, Opcodes.SIPUSH };

}
