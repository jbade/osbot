package com.iinc.osupdater.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.regex.Matcher;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.util.internet.InternetCallback;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class JarHandler {

	private final static String GAMEPACKS_DIRECTORY = "gamepacks/"; // directory where gamepacks are saved

	/**
	 * Generates the file path for a specific gamepack stored on the disk
	 * 
	 * @param revision
	 *            the revision of the gamepack
	 * @param deobfuscated
	 *            true if the gamepack has been deobfuscated </br>
	 *            false if the gamepack has not been deobfuscated
	 * @return String of the local filepath
	 */
	public static String path(int revision, boolean deobfuscated) {
		return GAMEPACKS_DIRECTORY + "gamepack" + revision + (deobfuscated ? ".deob" : "") + ".jar";
	}

	public static Set<Integer> getAvailableRevisions() {
		Set<Integer> revisions = new HashSet<Integer>();
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("gamepack([0-9]+)\\.jar$");

		for (File file : new File(GAMEPACKS_DIRECTORY).listFiles()) {
			Matcher m = pattern.matcher(file.getName());
			if (m.find()) {
				revisions.add(Integer.parseInt(m.group(1)));
			}
		}

		return revisions;
	}

	/**
	 * Loads the ClassNodes of a gamepack stored on the disk
	 * 
	 * @param revision
	 *            the revision of the gamepack
	 * @param deobfuscated
	 *            true if the gamepack has been deobfuscated </br>
	 *            false if the gamepack has not been deobfuscated
	 * @return HashMap representing all classes in the gamepack </br>
	 *         key=[name of the class], value=[ClassNode represented the class]
	 */
	public static HashMap<String, ClassNode> loadGamepack(int revision, boolean deobfuscated) {
		try {
			JarFile jar = new JarFile(path(revision, deobfuscated));
			return loadGamepack(jar);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Loads the ClassNodes of a gamepack stored on the disk
	 * 
	 * @param jar
	 *            JarFile of the gamepack
	 * @return HashMap representing all classes in the gamepack </br>
	 *         key=[name of the class], value=[ClassNode represented the class]
	 */
	private static HashMap<String, ClassNode> loadGamepack(JarFile jar) {
		HashMap<String, ClassNode> classes = new HashMap<String, ClassNode>();
		jar.stream().forEach(entry -> {
			if (entry.getName().endsWith(".class")) {
				try {
					ClassReader reader = new ClassReader(jar.getInputStream(entry));
					ClassNode node = new ClassNode();
					reader.accept(node, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
					classes.put(node.name, node);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		return classes;
	}

	/**
	 * Saves a gamepack to the disk as a .jar file and automatically calculates the revision
	 * 
	 * @param classes
	 *            the classes that makeup the gamepack
	 * @param deobfuscated
	 *            true if the gamepack has been deobfuscated </br>
	 *            false if the gamepack has not been deobfuscated
	 */
	public static void saveGamepack(HashMap<String, ClassNode> classes, boolean deobfuscated) {
		try {
			JarOutputStream jos = new JarOutputStream(new FileOutputStream(new File(path(revision(classes.get("client")), deobfuscated))));
			classes.values().stream().forEach((cn) -> {
				ClassWriter cw = new ClassWriter(1);
				cn.accept(cw);
				JarEntry entry = new JarEntry(cn.name + ".class");
				try {
					jos.putNextEntry(entry);
					jos.write(cw.toByteArray());
					jos.closeEntry();
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			jos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Saves a gamepack to the disk as a .jar file
	 * 
	 * @param classes
	 *            the classes that makeup the gamepack
	 * @param revision
	 *            the revision of the gamepack
	 * @param deobfuscated
	 *            true if the gamepack has been deobfuscated </br>
	 *            false if the gamepack has not been deobfuscated
	 */
	public static void saveGamepack(HashMap<String, ClassNode> classes, int revision, boolean deobfuscated) {
		try {
			JarOutputStream jos = new JarOutputStream(new FileOutputStream(new File(path(revision, deobfuscated))));
			classes.values().stream().forEach((cn) -> {
				ClassWriter cw = new ClassWriter(1);
				cn.accept(cw);
				JarEntry entry = new JarEntry(cn.name + ".class");
				try {
					jos.putNextEntry(entry);
					jos.write(cw.toByteArray());
					jos.closeEntry();
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			jos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Checks to see if the current gamepack available on the servers is stored on the disk
	 * 
	 * @return true if the current gamepack is stored on the disk </br>
	 *         false if the current gamepack is on the disk
	 */
	public static boolean outdated() {
		File dir = new File(GAMEPACKS_DIRECTORY);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		// find most recent gamepack
		TreeMap<Integer, File> files = new TreeMap<Integer, File>();
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("gamepack([0-9]*).jar$");
		for (File file : dir.listFiles()) {
			if (file.isFile()) {
				Matcher matcher = pattern.matcher(file.getName());
				if (matcher.find()) {
					files.put(Integer.valueOf(matcher.group(1)), file);
				}
			}
		}

		if (files.isEmpty()) {
			return true;
		}

		int hash = hash(files.lastEntry().getValue());

		Crawler crawler = new Crawler();
		crawler.crawl();

		return hash != crawler.remoteHash();
	}

	/**
	 * Downloads and saves the current gamepack
	 * 
	 * @return HashMap representing all classes in the gamepack </br>
	 *         key=[name of the class], value=[ClassNode represented the class]
	 * @throws IOException
	 */
	public static HashMap<String, ClassNode> update(boolean print) throws IOException {
		Crawler crawler = new Crawler();
		if (!crawler.crawl()) {
			throw new IOException("Error crawling params");
		}

		boolean result = crawler.download("gamepack.jar", !print ? null : new InternetCallback() {
			int drawn = 0;
			char str[] = "Downloading gamepack...".toCharArray();

			public void onDownload(double percent) {
				if ((int) (str.length * percent) > drawn) {
					for (int i = drawn; i < (int) (str.length * percent); i++) {
						System.out.print(str[i]);
					}
					drawn = (int) (str.length * percent);
				}
			}

			public void onComplete() {
				System.out.println(" Complete");
			}
		});

		if (!result) {
			throw new IOException("Error downloading gamepack");
		}

		JarFile jar = new JarFile("gamepack.jar");
		HashMap<String, ClassNode> classes = loadGamepack(jar);
		jar.close();

		if (!new File(GAMEPACKS_DIRECTORY).exists())
			Files.createDirectory(Paths.get(GAMEPACKS_DIRECTORY));

		Files.move(new File("gamepack.jar").toPath(), new File(path(revision(classes.get("client")), false)).toPath(),
				StandardCopyOption.REPLACE_EXISTING);

		return classes;

	}

	/**
	 * Calculates the hashcode representing a specific .jar file. The .jar file must contain the original manifest that was used when
	 * downloaded
	 * 
	 * @param file
	 *            File representing the .jar file
	 * @return int hashcode
	 */
	private static int hash(File file) {
		try {
			URL url = file.toURI().toURL();
			try (JarInputStream stream = new JarInputStream(url.openStream())) {
				return stream.getManifest().hashCode();
			} catch (Exception ignored) {
				return -1;
			}
		} catch (MalformedURLException ignored) {
			return -1;
		}
	}

	/**
	 * Calculates the client version from a ClassNode representing the class 'client' of a gamepack
	 * 
	 * @param client
	 *            ClassNode representing the 'client' class
	 * @return int revision
	 */
	@SuppressWarnings("unchecked")
	public static int revision(ClassNode client) {
		for (MethodNode mn : (List<MethodNode>) client.methods) {
			if (mn.name.equals("init") && mn.desc.equals("()V")) {
				List<PatternMatchResult> result = REVISION_PATTERN.match(mn);
				if (!result.isEmpty()) {
					//System.out.println(((IntInsnNode) result.get(0).getCaptures().get(2)).operand);
					return ((IntInsnNode) result.get(0).getCaptures().get(2)).operand;
				}	
				//System.out.println("here");
			}
		}
		return -1;
	}
//PatternElement.b().opcodes(Opcodes.SIPUSH).b(),
	private static final Pattern REVISION_PATTERN = new Pattern(
			PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).capture().b(),
			PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).capture().b(),
			PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).capture().b());

}
