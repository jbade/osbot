package com.iinc.osupdater.util;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarInputStream;

import com.iinc.osupdater.util.internet.Internet;
import com.iinc.osupdater.util.internet.InternetCallback;

public class Crawler {

	public final Map<String, String> parameters = new HashMap<>();
	public final String home;
	private final String config;
	public double percent;

	public Crawler() {
		home = String.format("http://oldschool%d.runescape.com/", 46);
		config = home + "jav_config.ws";
	}

	/**
	 * Gets the game hash on the server.
	 *
	 * @return The game hash on the server.
	 */
	public int remoteHash() {
		try {
			URL url = new URL(home + parameters.get("initial_jar"));
			try (JarInputStream stream = new JarInputStream(url.openStream())) {
				return stream.getManifest().hashCode();
			} catch (Exception ignored) {
				return -1;
			}
		} catch (IOException ignored) {
			return -1;
		}
	}

	/**
	 * Reads the server configuration.
	 *
	 * @return <t>true</t> if the server configuration was read, otherwise <t>false</t>.
	 */
	public boolean crawl() {
		try {
			List<String> source = Internet.read(config);
			for (String line : source) {
				if (line.startsWith("param=")) {
					line = line.substring(6);
				}
				int idx = line.indexOf("=");
				if (idx == -1) {
					continue;
				}
				parameters.put(line.substring(0, idx), line.substring(idx + 1));
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Downloads the game jar to the given target.
	 *
	 * @param target
	 *            The path to download to.
	 * @param callback
	 *            The callback to be run upon download percentage change.
	 * @return <t>true</t> if the game was downloaded, otherwise <t>false</t>.
	 */
	public boolean download(String target, final InternetCallback callback) {
		return Internet.download(home + parameters.get("initial_jar"), target, callback) != null;
	}

	/**
	 * Downloads the game jar to the given target.
	 *
	 * @param target
	 *            The path to download to.
	 * @return <t>true</t> if the game was downloaded, otherwise <t>false</t>.
	 */
	public boolean download(String target) {
		return download(target, null);
	}

}