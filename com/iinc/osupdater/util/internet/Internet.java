package com.iinc.osupdater.util.internet;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;

public class Internet {

	public static List<String> read(String url) {
		LinkedList<String> list = new LinkedList<String>();
		try {
			HttpURLConnection connection = HttpURLConnection.class.cast(new URL(url).openConnection());
			BufferedReader stream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String read;
			while ((read = stream.readLine()) != null) {
				list.add(read);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return list;
	}

	public static File download(String location, String target, InternetCallback callback) {
		BufferedInputStream in = null;
		FileOutputStream out = null;

		try {
			URL url = new URL(location);
			URLConnection conn = url.openConnection();
			int size = conn.getContentLength();

			in = new BufferedInputStream(url.openStream());
			out = new FileOutputStream(target);
			byte data[] = new byte[1024];
			int count;
			double sumCount = 0.0;

			while ((count = in.read(data, 0, 1024)) != -1) {
				out.write(data, 0, count);

				sumCount += count;
				if (size > 0) {
					if (callback != null)
						callback.onDownload(sumCount / size);
				}
			}

			if (callback !=  null && sumCount == size) {
				callback.onComplete();
			}

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e3) {
					e3.printStackTrace();
				}
			if (out != null)
				try {
					out.close();
				} catch (IOException e4) {
					e4.printStackTrace();
				}
		}

		return new File(target);
	}

}
