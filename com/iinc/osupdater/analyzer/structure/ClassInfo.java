package com.iinc.osupdater.analyzer.structure;

import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

import org.objectweb.asm.tree.ClassNode;

public class ClassInfo implements Comparable<ClassInfo> {
	private String id; // the human name for the class
	private boolean identified = false; // if the class has been identified
	private ClassNode classNode = null; // the identified ClassNode
	private HashMap<String, FieldInfo> fieldInfos = new HashMap<String, FieldInfo>(); // all fields

	public ClassInfo(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public boolean isIdentified() {
		return identified;
	}

	public void setIdentified(boolean identified) {
		this.identified = identified;
	}

	public boolean isDone() {
		if (!identified)
			return false;

		for (FieldInfo fieldInfo : fieldInfos.values()) {
			if (!fieldInfo.isIdentified())
				return false;
		}
		return true;
	}

	public ClassNode getClassNode() {
		return classNode;
	}

	public Collection<FieldInfo> getFieldInfos() {
		return fieldInfos.values();
	}

	public FieldInfo getFieldInfo(String id) {
		FieldInfo result = fieldInfos.get(id);
		if (result == null)
			throw new RuntimeException("Attempting to get FieldInfo that does not exist: " + id);
		return result;
	}

	public void addFieldInfo(FieldInfo field) {
		if (fieldInfos.containsKey(field.getId()))
			throw new RuntimeException("FieldInfo already exists " + field.getId());
		fieldInfos.put(field.getId(), field);
	}

	public void identify(ClassNode classNode) {
		this.classNode = classNode;
		identified = true;
	}

	@Override
	public int compareTo(ClassInfo o) {
		return this.id.compareTo(o.id);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%s: %s\n", id, classNode.name));
		for (FieldInfo fi : fieldInfos.values().stream().sorted().collect(Collectors.toList()))
			sb.append("    " + fi.toString() + "\n");
		return sb.toString();
	}

}
