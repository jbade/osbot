package com.iinc.osupdater.analyzer.structure;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

public class FieldInfo implements Comparable<FieldInfo> {
	private String id;
	private boolean identified = false;
	private FieldNode fieldNode;
	private ClassNode classNode;
	private Integer multiplier = null;

	public FieldInfo(String id) {
		this.id = id;
	}

	public boolean isIdentified() {
		return identified;
	}

	public void setIdentified(boolean identified) {
		this.identified = identified;
	}

	public FieldNode getFieldNode() {
		return fieldNode;
	}

	public ClassNode getClassNode() {
		return classNode;
	}

	public Integer getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(Integer multiplier) {
		this.multiplier = multiplier;
	}

	public String getId() {
		return id;
	}

	public void identify(ClassNode classNode, FieldNode fieldNode) {
		this.classNode = classNode;
		this.fieldNode = fieldNode;
		identified = true;
	}

	
	@Override
	public int compareTo(FieldInfo o) {
		return this.id.compareTo(o.id);
	}
	
	@Override
	public String toString() {
		return String.format("%s: %s.%s(%s)%s", id, classNode.name, fieldNode.name, fieldNode.desc, ((multiplier == null) ? "" : " * " + multiplier));
	}

}
