package com.iinc.osupdater.analyzer.structure;

import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

public class StaticInfo {
	private HashMap<String, FieldInfo> fieldInfos = new HashMap<String, FieldInfo>();

	public Collection<FieldInfo> getFieldInfos() {
		return fieldInfos.values();
	}

	public FieldInfo getFieldInfo(String id) {
		FieldInfo result = fieldInfos.get(id);
		if (result == null)
			throw new RuntimeException("Attempting to get FieldInfo that does not exist.");
		return result;
	}

	public void addFieldInfo(FieldInfo field) {
		if (fieldInfos.containsKey(field.getId()))
			throw new RuntimeException("FieldInfo already exists " + field.getId());
		fieldInfos.put(field.getId(), field);
	}

	public boolean isDone() {
		for (FieldInfo fieldInfo : fieldInfos.values()) {
			if (!fieldInfo.isIdentified())
				return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Static\n");
		for (FieldInfo fi : fieldInfos.values().stream().sorted().collect(Collectors.toList()))
			sb.append("    " + fi.toString() + "\n");
		return sb.toString();
	}
}
