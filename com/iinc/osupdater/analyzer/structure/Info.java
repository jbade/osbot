package com.iinc.osupdater.analyzer.structure;

import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Info {
	private HashMap<String, ClassInfo> classInfos = new HashMap<String, ClassInfo>();
	private StaticInfo staticInfo = new StaticInfo();

	public Collection<ClassInfo> getClassInfos() {
		return classInfos.values();
	}

	public ClassInfo getClassInfo(String id) {
		ClassInfo result = classInfos.get(id);
		if (result == null)
			throw new RuntimeException("Attempting to get ClassInfo that does not exist: " + id);
		return result;
	}

	public void addClassInfo(ClassInfo classInfo) {
		if (classInfos.containsKey(classInfo.getId()))
			throw new RuntimeException("ClassInfo already exists " + classInfo.getId());
		classInfos.put(classInfo.getId(), classInfo);
	}

	public StaticInfo getStaticInfo() {
		return staticInfo;
	}

	public FieldInfo getFieldInfo(String classId, String fieldId) {
		if (classId == null) {
			return staticInfo.getFieldInfo(fieldId);
		} else {
			return getClassInfo(classId).getFieldInfo(fieldId);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (ClassInfo ci : classInfos.values().stream().sorted().collect(Collectors.toList()))
			sb.append(ci.toString() + "\n");
		sb.append(staticInfo.toString());
		return sb.toString();
	}
}
