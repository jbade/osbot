package com.iinc.osupdater.analyzer.multiplier;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.JarHandler;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class MultiplierCollector {

	public static void main(String args[]) {
		HashMap<String, ClassNode> classes = JarHandler.loadGamepack(118, true);
		MultiplierCollector mc = new MultiplierCollector();
		mc.getMultipliers(classes, true);
	}

	private static final BigInteger modulo = BigInteger.ONE.shiftLeft(32);

	public static int unoverflowDivide(int product, int divisor) {
		if (divisor == 0)
			throw new IllegalArgumentException("No solution");
		while ((divisor & 1) == 0) {
			if ((product & 1) == 1)
				throw new IllegalArgumentException("No solution");
			divisor >>= 1;
			product >>= 1;
		}
		BigInteger bigDivisor = BigInteger.valueOf(divisor);
		BigInteger bigProduct = BigInteger.valueOf(product);
		BigInteger bigInverse = bigDivisor.modInverse(modulo);
		BigInteger bigResult = bigInverse.multiply(bigProduct);
		return bigResult.intValue();
	}

	private final LinkedList<Pattern> patterns = new LinkedList<Pattern>();

	public MultiplierCollector() {
		// build patterns

		List<PatternElement> list6 = new LinkedList<PatternElement>();
		list6.add(PatternElement.b().opcodes(Opcodes.GETFIELD, Opcodes.GETSTATIC).capture().b());
		list6.add(PatternElement.b().opcodes(Opcodes.LDC).capture().b());
		list6.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		patterns.add(new Pattern(list6));
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, Integer> getMultipliers(HashMap<String, ClassNode> classes, boolean print) {
		HashMap<String, Multiplier> multipliers = new HashMap<String, Multiplier>();

		long startTime = System.currentTimeMillis();
		for (ClassNode cn : classes.values())
			for (MethodNode mn : (Iterable<MethodNode>) cn.methods)
				for (Pattern pattern : patterns)
					for (PatternMatchResult pmr : pattern.match(mn)) {
						ArrayList<AbstractInsnNode> captures = pmr.getCaptures();

						String owner = null;
						String name = null;
						int multiplier = 0;

						for (AbstractInsnNode ain : captures) {
							if (ain instanceof FieldInsnNode) {
								FieldInsnNode fin = (FieldInsnNode) ain;
								owner = fin.owner;
								name = fin.name;
							} else if (ain instanceof LdcInsnNode) {
								LdcInsnNode lin = (LdcInsnNode) ain;
								multiplier = (Integer) lin.cst;
							} else if (ain instanceof IntInsnNode) {
								IntInsnNode iin = (IntInsnNode) ain;
								multiplier = iin.operand;
							} else {
								System.out.println(ain.getClass());
							}
						}

						if (multiplier % 2 == 0) {
							continue;
						}

						// we can assume that small values probabily arent multipliers
						if (multiplier < 99999 && multiplier > -99999) {
							continue;
						}

						// get the class that actually contains the field (not inherited)
						ClassNode t = classes.get(owner);
						while (!AnalyzerUtil.hasField(t, name)) {
							t = classes.get(t.superName);
							if (t == null)
								break;
						}

						if (t != null) {
							owner = t.name;
							// System.out.println(owner + "." + name + " " + multiplier);
							Multiplier m = multipliers.get(owner + "." + name);

							if (m == null) {
								m = new Multiplier(owner, name);
								multipliers.put(owner + "." + name, m);
							}

							m.add(multiplier);
						}
					}

		int count = 0;

		// System.out.println();
		HashMap<String, Integer> result = new HashMap<String, Integer>();
		for (Multiplier m : multipliers.values()) {

			int max = 0;
			Set<Integer> most = new HashSet<Integer>();
			for (Entry<Integer, Integer> entry : m.getMultipliers().entrySet()) {
				if (entry.getValue() > max) {
					max = entry.getValue();
					most.clear();
					most.add(entry.getKey());
				} else if (entry.getValue() == max) {
					most.add(entry.getKey());
				}
			}

			int multi = gcd(toInt(most));

			// we can assume that small values probabily arent multipliers
			if (multi % 2 == 0 || multi < 99999 && multi > -99999) {
				// System.out.println(multi);
				// System.out.println(m.getOwner() + "." + m.getName() + " " + most);
			} else {
				result.put(m.getOwner() + "." + m.getName(), multi);
				count++;
			}
		}

		if (print)
			System.out.println(String.format("Collected %d multipliers in %d ms.\n", count, System.currentTimeMillis() - startTime));

		return result;
	}

	public int[] toInt(Set<Integer> set) {
		int[] a = new int[set.size()];
		int i = 0;
		for (Integer val : set)
			a[i++] = val;
		return a;
	}

	private static int gcd(int a, int b) {
		while (b > 0) {
			int temp = b;
			b = a % b; // % is remainder
			a = temp;
		}
		return a;
	}

	private static int gcd(int... input) {
		int result = input[0];
		for (int i = 1; i < input.length; i++)
			result = gcd(result, input[i]);
		return result;
	}
}
