package com.iinc.osupdater.analyzer.multiplier;

import java.util.Set;

import com.iinc.osupdater.util.Bag;

public class Multiplier {
	private String owner;
	private String name;
	private Bag<Integer> multipliers = new Bag<Integer>();

	public Multiplier(String owner, String name) {
		this.owner = owner;
		this.name = name;
	}

	public void add(int multi) {
		this.multipliers.add(multi);
	}

	public String getOwner() {
		return owner;
	}

	public String getName() {
		return name;
	}

	public Bag<Integer> getMultipliers() {
		return multipliers;
	}
	
}
