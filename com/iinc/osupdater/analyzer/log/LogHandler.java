package com.iinc.osupdater.analyzer.log;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.regex.Matcher;

import com.google.gson.Gson;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;

public class LogHandler {

	private final static String LOGS_DIRECTORY = "logs/";

	public static String path(int revision) {
		return LOGS_DIRECTORY + "log" + revision + ".json";
	}

	public static Set<Integer> getAvailableRevisions() {
		Set<Integer> revisions = new HashSet<Integer>();
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("log([0-9]+)\\.json$");

		for (File file : new File(LOGS_DIRECTORY).listFiles()) {
			Matcher m = pattern.matcher(file.getName());
			if (m.find()) {
				revisions.add(Integer.parseInt(m.group(1)));
			}
		}

		return revisions;
	}

	public static Log load(int revision) {
		try {
			String json = new String(Files.readAllBytes(Paths.get(path(revision))), Charset.defaultCharset());
			Gson gson = new Gson();
			Log log = gson.fromJson(json, Log.class);
			return log;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void save(Info info, int revision) {
		try {
			Log log = new Log(revision);
			for (ClassInfo ci : info.getClassInfos()) {
				ClassInfoLog cil = new ClassInfoLog(ci.getClassNode().name);
				log.getHooks().addClassInfoLog(ci.getId(), cil);
				for (FieldInfo fi : ci.getFieldInfos()) {
					cil.addFieldInfoLog(fi.getId(),
							new FieldInfoLog(fi.getClassNode().name, fi.getFieldNode().name, fi.getFieldNode().desc, fi.getMultiplier()));
				}
			}

			for (FieldInfo fi : info.getStaticInfo().getFieldInfos()) {
				log.getHooks().getStatic().addFieldInfoLog(fi.getId(),
						new FieldInfoLog(fi.getClassNode().name, fi.getFieldNode().name, fi.getFieldNode().desc, fi.getMultiplier()));
			}

			Gson gson = new Gson();
			String json = gson.toJson(log);
			Files.write(Paths.get(path(revision)), json.getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
