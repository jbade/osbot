package com.iinc.osupdater.analyzer.log;

public class Log {
	private int revision;
	private HooksLog hooks = new HooksLog();

	public Log(int revision){
		this.revision = revision;
		
	}
	public HooksLog getHooks() {
		return hooks;
	}

	public int getRevision() {
		return revision;
	}
}

