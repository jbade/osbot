package com.iinc.osupdater.analyzer.log;

import java.util.HashMap;

import com.google.gson.annotations.SerializedName;
import com.iinc.osupdater.analyzer.structure.ClassInfo;

public class HooksLog {
	private HashMap<String, ClassInfoLog> classes = new HashMap<String, ClassInfoLog>();
	@SerializedName("static")
	private StaticInfoLog global = new StaticInfoLog();

	public void addClassInfoLog(String id, ClassInfoLog classInfo) {
		classes.put(id, classInfo);
	}

	public boolean hasClassInfoLog(String id) {
		return classes.containsKey(id);
	}

	public ClassInfoLog getClassInfoLog(String id) {
		return classes.get(id);
	}

	public StaticInfoLog getStatic() {
		return global;
	}
}
