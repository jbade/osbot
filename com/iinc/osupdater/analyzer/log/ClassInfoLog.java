package com.iinc.osupdater.analyzer.log;

import java.util.HashMap;

public class ClassInfoLog {
	private String name;
	private HashMap<String, FieldInfoLog> fields = new HashMap<String, FieldInfoLog>();

	public ClassInfoLog(String name) {
		this.name = name;
	}

	public void addFieldInfoLog(String id, FieldInfoLog field) {
		fields.put(id, field);
	}

	public String getName() {
		return name;
	}
	
	public boolean hasFieldInfoLog(String id) {
		return fields.containsKey(id);
	}

	public FieldInfoLog getFieldInfoLog(String id) {
		return fields.get(id);
	}
	

}
