package com.iinc.osupdater.analyzer.log;

public class FieldInfoLog {
	private String className;
	private String name;
	private String desc;
	private Integer multiplier;

	public FieldInfoLog(String className, String name, String desc, Integer multiplier) {
		this.className = className;
		this.name = name;
		this.desc = desc;
		this.multiplier = multiplier;
	}

	public String getClassName() {
		return className;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public Integer getMultiplier() {
		return multiplier;
	}

}
