package com.iinc.osupdater.analyzer.log;

import java.util.HashMap;

public class StaticInfoLog {
	private HashMap<String, FieldInfoLog> fields = new HashMap<String, FieldInfoLog>();

	public void addFieldInfoLog(String id, FieldInfoLog field) {
		fields.put(id, field);
	}
}
