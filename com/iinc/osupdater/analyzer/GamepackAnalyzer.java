package com.iinc.osupdater.analyzer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.analyzers.Analyzer;
import com.iinc.osupdater.analyzer.analyzers.BoundaryObjectAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.CharacterAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.ClientAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.GameObjectAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.GrandExchangeOfferAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.GroundItemAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.NpcAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.NpcDefinitionAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.PlayerAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.RegionAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.RenderableAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.SceneTileAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.WidgetAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.WidgetNodeAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.collection.BufferAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.collection.DualNodeAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.collection.HashTableAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.collection.LinkedListAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.collection.NodeAnalyzer;
import com.iinc.osupdater.analyzer.analyzers.statik.StaticFieldAnalyzer;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.multiplier.MultiplierCollector;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;

public class GamepackAnalyzer {
	private LinkedList<Analyzer> analyzers;
	
	public GamepackAnalyzer() {
		analyzers = new LinkedList<Analyzer>();
		analyzers.add(new NodeAnalyzer());
		analyzers.add(new BufferAnalyzer());
		analyzers.add(new HashTableAnalyzer());
		analyzers.add(new DualNodeAnalyzer());
		analyzers.add(new LinkedListAnalyzer());
		analyzers.add(new RenderableAnalyzer());
		analyzers.add(new CharacterAnalyzer());
		analyzers.add(new NpcAnalyzer());
		analyzers.add(new PlayerAnalyzer());
		analyzers.add(new GroundItemAnalyzer());
		analyzers.add(new NpcDefinitionAnalyzer());
		analyzers.add(new GrandExchangeOfferAnalyzer());
		analyzers.add(new WidgetAnalyzer());
		analyzers.add(new WidgetNodeAnalyzer());
		analyzers.add(new RegionAnalyzer());
		analyzers.add(new BoundaryObjectAnalyzer());
		analyzers.add(new GameObjectAnalyzer());
		analyzers.add(new SceneTileAnalyzer());
		analyzers.add(new StaticFieldAnalyzer());
		analyzers.add(new ClientAnalyzer());
	}

	/**
	 * Analyzes the gamepack
	 * 
	 * @param classes
	 */
	public Info run(HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		Info info = new Info();

		for (Analyzer a : analyzers)
			a.initialize(info, classes);

		int pass = 0;
		LinkedList<Analyzer> queue = new LinkedList<Analyzer>(analyzers);
		while (!queue.isEmpty()) {
			if (print)
				System.out.println("Pass " + ++pass);
			ListIterator<Analyzer> iterator = queue.listIterator();
			long startTime = System.currentTimeMillis();
			while (iterator.hasNext()) {
				Analyzer analyzer = iterator.next();
				if (analyzer.run(info, classes, print))
					iterator.remove();
			}
			if (print)
				System.out.println("(" + (System.currentTimeMillis() - startTime) + " ms)\n");
		}

		MultiplierCollector mc = new MultiplierCollector();
		HashMap<String, Integer> multipliers = mc.getMultipliers(classes, print);
		for (ClassInfo ci : info.getClassInfos())
			for (FieldInfo fi : ci.getFieldInfos())
				if (fi.getMultiplier() == null && multipliers.containsKey(fi.getClassNode().name + "." + fi.getFieldNode().name))
					fi.setMultiplier(multipliers.get(fi.getClassNode().name + "." + fi.getFieldNode().name));
		for (FieldInfo fi : info.getStaticInfo().getFieldInfos())
			if (fi.getMultiplier() == null && multipliers.containsKey(fi.getClassNode().name + "." + fi.getFieldNode().name))
				fi.setMultiplier(multipliers.get(fi.getClassNode().name + "." + fi.getFieldNode().name));
		
		return info;
	}
}
