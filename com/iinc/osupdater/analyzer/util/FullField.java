package com.iinc.osupdater.analyzer.util;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

public class FullField {
	public final ClassNode classNode;
	public final FieldNode fieldNode;

	public FullField(ClassNode classNode, FieldNode fieldNode) {
		super();
		this.classNode = classNode;
		this.fieldNode = fieldNode;
	}

	public ClassNode getClassNode() {
		return classNode;
	}

	public FieldNode getFieldNode() {
		return fieldNode;
	}

}
