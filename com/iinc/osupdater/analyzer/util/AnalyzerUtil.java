package com.iinc.osupdater.analyzer.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

public class AnalyzerUtil {

	@SuppressWarnings("unchecked")
	public static List<MethodNode> allStaticMethods(Collection<ClassNode> classes) {
		LinkedList<MethodNode> methods = new LinkedList<MethodNode>();
		for (ClassNode cn : classes) {
			for (MethodNode mn : (List<MethodNode>) cn.methods) {
				if (hasAccessFlags(mn, Opcodes.ACC_STATIC))
					methods.add(mn);
			}
		}

		return methods;
	}

	@SuppressWarnings("unchecked")
	public static List<FullField> allStaticFields(Collection<ClassNode> classes) {
		LinkedList<FullField> fields = new LinkedList<FullField>();
		for (ClassNode cn : classes) {
			for (FieldNode fn : (List<FieldNode>) cn.fields) {
				if (hasAccessFlags(fn, Opcodes.ACC_STATIC))
					fields.add(new FullField(cn, fn));
			}
		}

		return fields;
	}

	public static boolean hasField(ClassNode cn, String name) {
		return getField(cn, name) != null;
	}

	@SuppressWarnings("unchecked")
	public static FieldNode getField(ClassNode cn, String name) {
		for (FieldNode fn : (Iterable<FieldNode>) cn.fields) {
			if (fn.name.equals(name))
				return fn;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static int countInstanceFields(ClassNode cn, String desc) {
		int result = 0;
		for (FieldNode fn : (Iterable<FieldNode>) cn.fields) {
			if (fn.desc.equals(desc) && !hasAccessFlags(fn, Opcodes.ACC_STATIC))
				result++;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static int countInstanceFields(ClassNode cn) {
		int count = 0;
		for (FieldNode fn : (Iterable<FieldNode>) cn.fields) {
			if (!hasAccessFlags(fn, Opcodes.ACC_STATIC))
				count++;
		}
		return count;
	}

	public static boolean hasAccessFlags(int access, int... flags) {
		for (int flag : flags)
			if ((access & flag) == 0)
				return false;
		return true;
	}

	public static boolean hasAccessFlags(ClassNode cn, int... flags) {
		return hasAccessFlags(cn.access, flags);
	}

	public static boolean hasAccessFlags(FieldNode fn, int... flags) {
		return hasAccessFlags(fn.access, flags);
	}

	public static boolean hasAccessFlags(MethodNode mn, int... flags) {
		return hasAccessFlags(mn.access, flags);
	}

}
