package com.iinc.osupdater.analyzer.exceptions;

public class IdentificationException extends Exception {

	public IdentificationException(String classId) {
		super("Unable to identify class " + classId);
	}

	public IdentificationException(String classId, String fieldId) {
		super("Unable to identify field " + ((classId == null) ? "" : classId + ".") + fieldId);
	}

}
