package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;

public class SceneTileAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public SceneTileAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// SceneTile
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("SceneTile").isIdentified() && info.getClassInfo("Region").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> extendsNode = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Node").getClassNode().name)).collect(Collectors.toList());

				List<FieldNode> filtered = ((List<FieldNode>) info.getClassInfo("Region").getClassNode().fields).stream()
						.filter((fn) -> extendsNode.parallelStream().anyMatch((en) -> fn.desc.equals("[[[L" + en.name + ";")))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					FieldNode fn = filtered.get(0);
					ClassNode cn = classes.get(fn.desc.substring(4, fn.desc.length() - 1));

					info.getClassInfo("Region").getFieldInfo("sceneTiles").identify(info.getClassInfo("Region").getClassNode(), fn);
					info.getClassInfo("SceneTile").identify(cn);
					if (print)
						System.out.println("Identified: SceneTile");
				} else {
					for (FieldNode fn : filtered)
						System.out.println(fn.name);
					throw new IdentificationException("SceneTile");
				}
			}

		});

		// boundaryObject
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getFieldInfo("SceneTile", "boundaryObject").isIdentified() && info.getClassInfo("SceneTile").isIdentified()
						&& info.getClassInfo("BoundaryObject").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("SceneTile").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> fn.desc.equals("L" + info.getClassInfo("BoundaryObject").getClassNode().name + ";")
								&& !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					FieldNode fn = filtered.get(0);
					info.getFieldInfo("SceneTile", "boundaryObject").identify(cn, fn);
					if (print)
						System.out.println("Identified: SceneTile.boundaryObject");
				} else {
					throw new IdentificationException("SceneTile", "boundaryObject");
				}
			}

		});

		// gameObjects
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getFieldInfo("SceneTile", "gameObjects").isIdentified() && info.getClassInfo("SceneTile").isIdentified()
						&& info.getClassInfo("GameObject").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("SceneTile").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> fn.desc.equals("[L" + info.getClassInfo("GameObject").getClassNode().name + ";")
								&& !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					FieldNode fn = filtered.get(0);
					info.getFieldInfo("SceneTile", "gameObjects").identify(cn, fn);
					if (print)
						System.out.println("Identified: SceneTile.gameObjects");
				} else {
					throw new IdentificationException("SceneTile", "gameObjects");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("SceneTile");
		classInfo.addFieldInfo(new FieldInfo("boundaryObject"));
		classInfo.addFieldInfo(new FieldInfo("gameObjects"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("SceneTile").isDone();
	}

}
