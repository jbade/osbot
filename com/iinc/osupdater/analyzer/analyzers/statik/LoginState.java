package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.analyzer.util.FullField;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class LoginState implements Identifier {

	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "loginState").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		List<PatternElement> list = new LinkedList<PatternElement>();
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).capture().b());
		list.add(PatternElement.b().opcodes(Opcodes.PUTSTATIC).capture().b());
		Pattern pattern = new Pattern(list);

		List<FullField> results = new ArrayList<>();
		for (ClassNode cn : classes.values()) {
			for (MethodNode mn : (List<MethodNode>) cn.methods) {
				if (mn.name.equals("<clinit>")) {

					List<PatternMatchResult> matches = pattern.match(mn);
					for (PatternMatchResult pmr : matches) {
						//System.out.println(pmr);
					}

					if (matches.size() >= 18) {
						//System.out.println(cn.name + "." + mn.name);

						FieldInsnNode loginState = (FieldInsnNode) matches.get(9).getCaptures().get(1);
						//System.out.println(loginState.owner + "." + loginState.name);

						FieldInsnNode abc = (FieldInsnNode) matches.get(21).getCaptures().get(1);
						AbstractInsnNode abcLic = matches.get(21).getCaptures().get(0);

						boolean success = true;
						success = success && loginState.desc.equals("I");
						success = success && abc.desc.equals("Ljava/lang/String;");
						if (abcLic instanceof LdcInsnNode) {
							success = success
									&& ("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:'@#~,<.>/?\\| "
											.equals(((LdcInsnNode) abcLic).cst));
						} else {
							success = false;
						}

						if (success) {
							results.add(new FullField(cn, AnalyzerUtil.getField(classes.get(loginState.owner), loginState.name)));
						}
					}

				}
			}
		}

		if (results.size() != 1) {
			System.out.println(results.size());
			throw new IdentificationException(null, "loginState");
		}

		info.getFieldInfo(null, "loginState").identify(results.get(0).classNode, results.get(0).fieldNode);
		if (print) {
			System.out.println("Identified: loginState");
		}
	}

}