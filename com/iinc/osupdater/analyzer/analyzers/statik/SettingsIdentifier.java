package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class SettingsIdentifier implements Identifier {
	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "settings").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		List<MethodNode> filtered = AnalyzerUtil.allStaticMethods(classes.values()).stream().filter((mn) -> mn.name.equals("<clinit>"))
				.collect(Collectors.toList());

		List<PatternMatchResult> matches = new ArrayList<>();

		List<PatternElement> list = new LinkedList<PatternElement>();
		list.add(PatternElement.b().opcodes(Opcodes.SIPUSH).capture().b()); // 2000
		list.add(PatternElement.b().opcodes(Opcodes.NEWARRAY).b());
		list.add(PatternElement.b().opcodes(Opcodes.PUTSTATIC).capture().b()); // settings
		list.add(PatternElement.b().opcodes(Opcodes.SIPUSH).capture().b()); // 2000
		list.add(PatternElement.b().opcodes(Opcodes.NEWARRAY).b());
		list.add(PatternElement.b().opcodes(Opcodes.PUTSTATIC).capture().b()); // ????
		Pattern pattern = new Pattern(list);

		for (MethodNode mn : filtered) {
			List<PatternMatchResult> l = pattern.match(mn);
			if (l.size() == 1) {
				IntInsnNode iin1 = (IntInsnNode) l.get(0).getCaptures().get(0);
				IntInsnNode iin2 = (IntInsnNode) l.get(0).getCaptures().get(2);
				if (iin1.operand == 2000 && iin2.operand == 2000) {
					matches.addAll(l);
				}
			}
		}

		// System.out.println(matches.size());

		if (matches.size() != 1) {
			throw new IdentificationException(null, "settings");
		}

		FieldInsnNode finSettings = (FieldInsnNode) matches.get(0).getCaptures().get(1);

		ClassNode cnSettings = classes.get(finSettings.owner);

		FieldNode settings = AnalyzerUtil.getField(cnSettings, finSettings.name);

		boolean desc = true;
		desc = desc && settings.desc.equals("[I");

		if (!desc) {
			throw new IdentificationException(null, "settings");
		}

		info.getFieldInfo(null, "settings").identify(cnSettings, settings);
		if (print) {
			System.out.println("Identified: settings");
		}
	}
}
