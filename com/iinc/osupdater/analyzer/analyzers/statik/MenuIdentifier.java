package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class MenuIdentifier implements Identifier {
	Pattern pattern1, pattern2, pattern3;

	public MenuIdentifier() {
		List<PatternElement> list1 = new LinkedList<PatternElement>();
		list1.add(PatternElement.b().opcodes(Opcodes.ICONST_0).b());
		list1.add(PatternElement.b().opcodes(Opcodes.PUTSTATIC).capture().b()); // menuVisible
		list1.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // menuX
		list1.add(PatternElement.b().opcodes(Opcodes.LDC).b());
		list1.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list1.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // menuY
		list1.add(PatternElement.b().opcodes(Opcodes.LDC).b());
		list1.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list1.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // menuWidth
		list1.add(PatternElement.b().opcodes(Opcodes.LDC).b());
		list1.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list1.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // menuHeight
		list1.add(PatternElement.b().opcodes(Opcodes.LDC).b());
		list1.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		// list1.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b()); // dummy param??
		// list1.add(PatternElement.b().opcodes(Opcodes.INVOKESTATIC).b());
		pattern1 = new Pattern(list1);

		List<PatternElement> list2 = new LinkedList<PatternElement>();

		pattern2 = new Pattern(list2);
	}

	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "menuX").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		pattern1(info, classes, print);

	}

	@SuppressWarnings("unchecked")
	private void pattern1(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		// 118 bp.bd
		boolean identified = false;
		for (ClassNode cn : classes.values()) {
			if (identified)
				break;
			for (MethodNode mn : (List<MethodNode>) cn.methods) {
				List<PatternMatchResult> matches1 = pattern1.match(mn);

				if (matches1.size() > 0) { // all matches are the same
					// System.out.println(cn.name + " " + mn.name + " " + matches1.size());

					FieldInsnNode menuVisible = (FieldInsnNode) matches1.get(0).getCaptures().get(0);
					FieldInsnNode menuX = (FieldInsnNode) matches1.get(0).getCaptures().get(1);
					FieldInsnNode menuY = (FieldInsnNode) matches1.get(0).getCaptures().get(2);
					FieldInsnNode menuWidth = (FieldInsnNode) matches1.get(0).getCaptures().get(3);
					FieldInsnNode menuHeight = (FieldInsnNode) matches1.get(0).getCaptures().get(4);

					if (menuVisible.desc.equals("Z") && menuX.desc.equals("I") && menuY.desc.equals("I") && menuHeight.desc.equals("I")
							&& menuWidth.desc.equals("I")) {

						FieldNode menuVisibleFn = AnalyzerUtil.getField(classes.get(menuVisible.owner), menuVisible.name);
						FieldNode menuXFn = AnalyzerUtil.getField(classes.get(menuX.owner), menuX.name);
						FieldNode menuYFn = AnalyzerUtil.getField(classes.get(menuY.owner), menuY.name);
						FieldNode menuWidthFn = AnalyzerUtil.getField(classes.get(menuWidth.owner), menuWidth.name);
						FieldNode menuHeightFn = AnalyzerUtil.getField(classes.get(menuHeight.owner), menuHeight.name);
						// System.out.println("menuVisible " + classes.get(menuVisible.owner).name + "." + menuVisible.name);

						info.getFieldInfo(null, "menuVisible").identify(classes.get(menuVisible.owner), menuVisibleFn);
						info.getFieldInfo(null, "menuX").identify(classes.get(menuX.owner), menuXFn);
						info.getFieldInfo(null, "menuY").identify(classes.get(menuY.owner), menuYFn);
						info.getFieldInfo(null, "menuWidth").identify(classes.get(menuWidth.owner), menuWidthFn);
						info.getFieldInfo(null, "menuHeight").identify(classes.get(menuHeight.owner), menuHeightFn);

						if (print) {
							System.out.println("Identified: menuVisible");
							System.out.println("Identified: menuX");
							System.out.println("Identified: menuY");
							System.out.println("Identified: menuWidth");
							System.out.println("Identified: menuHeight");
						}
						identified = true;
						break;
					}
				}
			}
		}

		if (!identified) {
			throw new IdentificationException(null, "menuX");
		}
	}
}