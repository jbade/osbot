package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.analyzer.util.FullField;

public class LocalPlayerIdentifier implements Identifier {
	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "localPlayer").isIdentified() && info.getClassInfo("Player").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		ClassNode player = info.getClassInfo("Player").getClassNode();
		List<FullField> filtered = AnalyzerUtil.allStaticFields(classes.values()).stream()
				.filter((f) -> f.getFieldNode().desc.equals("L" + player.name + ";"))
				.collect(Collectors.toList());

		if (filtered.size() != 1) {
			throw new IdentificationException(null, "localPlayer");
		}
		
		info.getFieldInfo(null, "localPlayer").identify(filtered.get(0).getClassNode(), filtered.get(0).getFieldNode());
		if (print) {
			System.out.println("Identified: localPlayer");
		}
	}

}