package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.HashMap;
import java.util.LinkedList;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.analyzers.Analyzer;
import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;

public class StaticFieldAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public StaticFieldAnalyzer() {
		identifiers = new LinkedList<Identifier>();
		identifiers.add(new MenuIdentifier());
		identifiers.add(new NpcsIdentifier());
		identifiers.add(new LoginState());
		identifiers.add(new ClientClinitIdentifier());
		identifiers.add(new WidgetNodeCacheIdentifier());
		identifiers.add(new WidgetsIdentifier());
		identifiers.add(new BaseXYIdentifier());
		identifiers.add(new LocalPlayerIdentifier());
		identifiers.add(new PlaneGroundItemsIdentifier());
		identifiers.add(new CameraIdentifier());
		identifiers.add(new TileHeightsSettingsIdentifier());
		identifiers.add(new SettingsIdentifier());
		identifiers.add(new RegionIdentifier());
		
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		info.getStaticInfo().addFieldInfo(new FieldInfo("npcs"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("menuVisible"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("menuX"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("menuY"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("menuWidth"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("menuHeight"));
		
		info.getStaticInfo().addFieldInfo(new FieldInfo("gameState"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("widgetNodeCache"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("npcIndices"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("widgetBoundsX"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("widgetBoundsY"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("widgetBoundsWidth"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("widgetBoundsHeight"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("viewportHeight"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("viewportWidth"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("viewportScale"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("grandExchangeOffers"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("menuActions"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("menuOptions"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("menuCount"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("mapAngle"));
	//	info.getStaticInfo().addFieldInfo(new FieldInfo("mapOffset"));
	//	info.getStaticInfo().addFieldInfo(new FieldInfo("mapScale"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("players"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("groundItems"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("currentLevels"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("levels"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("widgets"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("baseY"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("baseX"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("localPlayer"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("plane"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("cameraX"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("cameraY"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("cameraZ"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("cameraPitch"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("cameraYaw"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("tileSettings"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("tileHeights"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("settings"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("destinationX"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("destinationY"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("region"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("spellSelected"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("itemSelectionState"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("loginState"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("experiences"));
		info.getStaticInfo().addFieldInfo(new FieldInfo("cursorState"));
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getStaticInfo().isDone();
	}

}
