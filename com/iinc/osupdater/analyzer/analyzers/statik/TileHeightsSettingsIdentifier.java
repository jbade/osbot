package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class TileHeightsSettingsIdentifier implements Identifier {
	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "tileHeights").isIdentified() || !info.getFieldInfo(null, "tileSettings").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		List<MethodNode> filtered = AnalyzerUtil.allStaticMethods(classes.values()).stream().filter((mn) -> mn.name.equals("<clinit>"))
				.collect(Collectors.toList());

		List<PatternMatchResult> matches = new ArrayList<>();

		List<PatternElement> list = new LinkedList<PatternElement>();
		list.add(PatternElement.b().opcodes(Opcodes.ICONST_4).b());
		list.add(PatternElement.b().opcodes(Opcodes.BIPUSH).capture().b()); // 104/105
		list.add(PatternElement.b().opcodes(Opcodes.BIPUSH).capture().b()); // 104/105
		list.add(PatternElement.b().opcodes(Opcodes.MULTIANEWARRAY).b());
		list.add(PatternElement.b().opcodes(Opcodes.PUTSTATIC).capture().b());
		Pattern pattern = new Pattern(list);

		for (MethodNode mn : filtered) {
			List<PatternMatchResult> l = pattern.match(mn);
			if (l.size() == 2) {
				// System.out.println(mn.name);
				matches.addAll(l);
			}

		}

		// System.out.println(matches.size());
		FieldInsnNode finTileHeights = null;
		FieldInsnNode finTileSettings = null;
		for (PatternMatchResult pmr : matches) {
			IntInsnNode iin1 = (IntInsnNode) pmr.getCaptures().get(0);
			IntInsnNode iin2 = (IntInsnNode) pmr.getCaptures().get(1);
			FieldInsnNode fin = (FieldInsnNode) pmr.getCaptures().get(2);
			// System.out.println(fin.owner + "." + fin.name + " " + fin.desc + " " + iin1.operand + " " + iin2.operand);

			if (fin.desc.equals("[[[I") && iin1.operand == 105 && iin2.operand == 105) {
				if (finTileHeights != null) {
					throw new IdentificationException(null, "tileHeights");
				}
				finTileHeights = fin;
			}

			if (fin.desc.equals("[[[B") && iin1.operand == 104 && iin2.operand == 104) {
				if (finTileSettings != null) {
					throw new IdentificationException(null, "tileSettings");
				}
				finTileSettings = fin;
			}
		}

		if (finTileHeights == null || finTileSettings == null) {
			throw new IdentificationException(null, "tileHeights");
		}

		FieldNode fnTileHeights = AnalyzerUtil.getField(classes.get(finTileHeights.owner), finTileHeights.name);
		FieldNode fnTileSettings = AnalyzerUtil.getField(classes.get(finTileSettings.owner), finTileSettings.name);

		ClassNode cnTileHeights = classes.get(finTileHeights.owner);
		ClassNode cnTileSettings = classes.get(finTileSettings.owner);

		info.getFieldInfo(null, "tileHeights").identify(cnTileHeights, fnTileHeights);
		info.getFieldInfo(null, "tileSettings").identify(cnTileSettings, fnTileSettings);

		if (print) {
			System.out.println("Identified: tileHeights");
			System.out.println("Identified: tileSettings");
		}
	}
}
