package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class CameraIdentifier implements Identifier {
	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "cameraX").isIdentified() || !info.getFieldInfo(null, "cameraY").isIdentified()
				|| !info.getFieldInfo(null, "cameraZ").isIdentified() || !info.getFieldInfo(null, "cameraPitch").isIdentified()
				|| !info.getFieldInfo(null, "cameraYaw").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		// 123 fy.at(III)V

		List<MethodNode> filtered = AnalyzerUtil.allStaticMethods(classes.values()).stream().filter((mn) -> mn.desc.equals("(III)V"))
				.collect(Collectors.toList());

		List<PatternMatchResult> matches = new ArrayList<>();
		/*
		 		n -= cameraX * 478773107;
		        final int n5 = n4 - cameraZ * 1698302853;
		        n2 -= cameraY * 297313415;
		        final int n6 = cf.ax[cameraPitch * -1257185177];
		        final int n7 = cf.ac[cameraPitch * -1257185177];
		        final int n8 = cf.ax[cameraYaw * -530970853];
		        final int n9 = cf.ac[cameraYaw * -530970853];
		 */
		List<PatternElement> list = new LinkedList<PatternElement>();

		// 41 iload_0
		// 42 getstatic #30 <n/fr I>
		// 45 ldc #31 <478773107>
		// 47 imul
		// 48 isub
		// 49 istore_0
		// 50 iload 4
		// 52 getstatic #36 <f/fj I>
		// 55 ldc #37 <1698302853>
		// 57 imul
		// 58 isub
		// 59 istore 4
		// 61 iload_1
		// 62 getstatic #41 <ez/fy I>
		// 65 ldc #42 <297313415>
		// 67 imul
		// 68 isub
		// 69 istore_1
		// 70 getstatic #47 <cf/ax [I>
		// 73 getstatic #52 <dp/fg I>
		// 76 ldc #53 <-1257185177>
		// 78 imul
		// 79 iaload
		// 80 istore 5
		// 82 getstatic #56 <cf/ac [I>
		// 85 getstatic #52 <dp/fg I>
		// 88 ldc #53 <-1257185177>
		// 90 imul
		// 91 iaload
		// 92 istore 6
		// 94 getstatic #47 <cf/ax [I>
		// 97 getstatic #61 <fz/ft I>
		// 100 ldc #62 <-530970853>
		// 102 imul
		// 103 iaload
		// 104 istore 7
		// 106 getstatic #56 <cf/ac [I>
		// 109 getstatic #61 <fz/ft I>
		// 112 ldc #62 <-530970853>
		// 114 imul
		// 115 iaload
		// 116 istore 8

		list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // cameraX
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISUB).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISTORE).b());

		list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // cameraZ
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISUB).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISTORE).b());

		list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // cameraY
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISUB).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISTORE).b());

		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).b());
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // cameraPitch
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list.add(PatternElement.b().opcodes(Opcodes.IALOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISTORE).b());
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).b());
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).b());
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list.add(PatternElement.b().opcodes(Opcodes.IALOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISTORE).b());

		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).b());
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // cameraYaw
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list.add(PatternElement.b().opcodes(Opcodes.IALOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISTORE).b());
		Pattern pattern = new Pattern(list);

		for (MethodNode mn : filtered) {
			List<PatternMatchResult> l = pattern.match(mn);
			if (l.size() == 1) {
				// System.out.println(mn.name + " " + mn.desc);
				matches.addAll(l);
			}

		}

		// System.out.println(matches.size());

		if (matches.size() != 1) {
			throw new IdentificationException(null, "cameraX");
		}

		FieldInsnNode finCameraX = (FieldInsnNode) matches.get(0).getCaptures().get(0);
		FieldInsnNode finCameraY = (FieldInsnNode) matches.get(0).getCaptures().get(2);
		FieldInsnNode finCameraZ = (FieldInsnNode) matches.get(0).getCaptures().get(1);
		FieldInsnNode finCameraPitch = (FieldInsnNode) matches.get(0).getCaptures().get(3);
		FieldInsnNode finCameraYaw = (FieldInsnNode) matches.get(0).getCaptures().get(4);

		ClassNode cnCameraX = classes.get(finCameraX.owner);
		ClassNode cnCameraY = classes.get(finCameraY.owner);
		ClassNode cnCameraZ = classes.get(finCameraZ.owner);
		ClassNode cnCameraPitch = classes.get(finCameraPitch.owner);
		ClassNode cnCameraYaw = classes.get(finCameraYaw.owner);

		FieldNode cameraX = AnalyzerUtil.getField(cnCameraX, finCameraX.name);
		FieldNode cameraY = AnalyzerUtil.getField(cnCameraY, finCameraY.name);
		FieldNode cameraZ = AnalyzerUtil.getField(cnCameraZ, finCameraZ.name);
		FieldNode cameraPitch = AnalyzerUtil.getField(cnCameraPitch, finCameraPitch.name);
		FieldNode cameraYaw = AnalyzerUtil.getField(cnCameraYaw, finCameraYaw.name);

		boolean desc = true;
		desc = desc && cameraX.desc.equals("I");
		desc = desc && cameraY.desc.equals("I");
		desc = desc && cameraZ.desc.equals("I");
		desc = desc && cameraPitch.desc.equals("I");
		desc = desc && cameraYaw.desc.equals("I");

		if (!desc) {
			throw new IdentificationException(null, "cameraX");
		}

		info.getFieldInfo(null, "cameraX").identify(cnCameraX, cameraX);
		info.getFieldInfo(null, "cameraY").identify(cnCameraY, cameraY);
		info.getFieldInfo(null, "cameraZ").identify(cnCameraZ, cameraZ);
		info.getFieldInfo(null, "cameraPitch").identify(cnCameraPitch, cameraPitch);
		info.getFieldInfo(null, "cameraYaw").identify(cnCameraYaw, cameraYaw);

		if (print) {
			System.out.println("Identified: cameraX");
			System.out.println("Identified: cameraY");
			System.out.println("Identified: cameraZ");
			System.out.println("Identified: cameraPitch");
			System.out.println("Identified: cameraYaw");
		}
	}
}