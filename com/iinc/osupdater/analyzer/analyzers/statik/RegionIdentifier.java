package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.analyzer.util.FullField;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class RegionIdentifier implements Identifier {
	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "region").isIdentified() && info.getClassInfo("Region").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		ClassNode cn = classes.get("client");
		
		List<FullField> filtered = AnalyzerUtil.allStaticFields(classes.values()).stream()
				.filter((fn) -> fn.fieldNode.desc.equals("L" + info.getClassInfo("Region").getClassNode().name + ";")).collect(Collectors.toList());
		
		if (filtered.size() == 1) {
			info.getFieldInfo(null, "region").identify(filtered.get(0).classNode, filtered.get(0).getFieldNode());
			if (print)
				System.out.println("Identified: region");
		} else {
			for (FullField fn : (List<FullField>) filtered)
				System.out.println(fn.getClassNode().name + "." + fn.getFieldNode().name);
			throw new IdentificationException("region");
		}
	}
}
