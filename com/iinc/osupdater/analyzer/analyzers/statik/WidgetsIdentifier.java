package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.analyzer.util.FullField;

public class WidgetsIdentifier implements Identifier {
	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "widgets").isIdentified() && info.getClassInfo("Widget").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		ClassNode widget = info.getClassInfo("Widget").getClassNode();
		List<FullField> filtered = AnalyzerUtil.allStaticFields(classes.values()).stream()
				.filter((f) -> f.getFieldNode().desc.equals("[[L" + widget.name + ";")).collect(Collectors.toList());

		if (filtered.size() != 1) {
			throw new IdentificationException(null, "widgets");
		}

		info.getFieldInfo(null, "widgets").identify(filtered.get(0).getClassNode(), filtered.get(0).getFieldNode());
		if (print) {
			System.out.println("Identified: widgets");
		}
	}

}
