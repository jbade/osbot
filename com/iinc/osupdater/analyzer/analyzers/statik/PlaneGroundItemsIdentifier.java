package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class PlaneGroundItemsIdentifier implements Identifier {
	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return (!info.getFieldInfo(null, "plane").isIdentified() || !info.getFieldInfo(null, "groundItems").isIdentified())
				&& info.getClassInfo("LinkedList").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		// 123 du.be(II)V
		List<MethodNode> filtered = AnalyzerUtil.allStaticMethods(classes.values()).stream().filter((mn) -> mn.desc.equals("(II)V"))
				.collect(Collectors.toList());

		List<PatternMatchResult> matches = new ArrayList<>();

		List<PatternElement> list = new LinkedList<PatternElement>();
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // groundItems
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b()); // plane
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list.add(PatternElement.b().opcodes(Opcodes.AALOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.AALOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.AALOAD).b());
		list.add(PatternElement.b().opcodes(Opcodes.ASTORE).b());
		Pattern pattern = new Pattern(list);

		for (MethodNode mn : filtered) {
			List<PatternMatchResult> l = pattern.match(mn);
			if (l.size() == 1) {
				matches.addAll(l);
			}
		}

		// System.out.println(matches.size());

		if (matches.size() != 1) {
			throw new IdentificationException(null, "plane");
		}

		FieldInsnNode finGroundItems = (FieldInsnNode) matches.get(0).getCaptures().get(0);
		FieldInsnNode finPlane = (FieldInsnNode) matches.get(0).getCaptures().get(1);

		ClassNode cnGroundItems = classes.get(finGroundItems.owner);
		ClassNode cnPlane = classes.get(finPlane.owner);

		FieldNode groundItems = AnalyzerUtil.getField(cnGroundItems, finGroundItems.name);
		FieldNode plane = AnalyzerUtil.getField(cnPlane, finPlane.name);

		boolean desc = true;
		desc = desc && groundItems.desc.equals("[[[L" + info.getClassInfo("LinkedList").getClassNode().name + ";");
		desc = desc && plane.desc.equals("I");

		if (!desc) {
			throw new IdentificationException(null, "plane");
		}

		info.getFieldInfo(null, "plane").identify(cnPlane, plane);
		info.getFieldInfo(null, "groundItems").identify(cnGroundItems, groundItems);
		if (print) {
			System.out.println("Identified: plane");
			System.out.println("Identified: groundItems");
		}
	}
}
