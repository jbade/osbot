package com.iinc.osupdater.analyzer.analyzers.statik;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class BaseXYIdentifier implements Identifier {
	@Override
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
		return !info.getFieldInfo(null, "baseX").isIdentified() || !info.getFieldInfo(null, "baseY").isIdentified();
	}

	@Override
	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		List<MethodNode> filtered = AnalyzerUtil.allStaticMethods(classes.values()).stream()// .filter((mn) -> mn.desc.startsWith("(II"))
				.collect(Collectors.toList());

		List<PatternMatchResult> matches = new ArrayList<>();

		List<PatternElement> list = new LinkedList<PatternElement>();
		list.add(PatternElement.b().opcodes(Opcodes.GETSTATIC).capture().b());
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISUB).b());
		list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
		list.add(PatternElement.b().opcodes(Opcodes.ISHL).capture().b());
		Pattern pattern = new Pattern(list);

		for (MethodNode mn : filtered) {
			List<PatternMatchResult> l = pattern.match(mn);
			if (l.size() == 2) {
				// System.out.println(mn.name);
				matches.addAll(l);
			}

		}
		// System.exit(0);

		// System.out.println(matches.size());
		if (matches.size() != 2) {
			throw new IdentificationException(null, "baseX");
		}

		FieldInsnNode finBaseX = (FieldInsnNode) matches.get(0).getCaptures().get(0);
		FieldInsnNode finBaseY = (FieldInsnNode) matches.get(1).getCaptures().get(0);

		ClassNode cnBaseX = classes.get(finBaseX.owner);
		ClassNode cnBaseY = classes.get(finBaseY.owner);

		FieldNode baseX = AnalyzerUtil.getField(cnBaseX, finBaseX.name);
		FieldNode baseY = AnalyzerUtil.getField(cnBaseY, finBaseY.name);

		boolean desc = true;
		desc = desc && baseX.desc.equals("I");
		desc = desc && baseY.desc.equals("I");

		if (!desc) {
			throw new IdentificationException(null, "baseX");
		}

		info.getFieldInfo(null, "baseX").identify(cnBaseX, baseX);
		info.getFieldInfo(null, "baseY").identify(cnBaseY, baseY);

		if (print) {
			System.out.println("Identified: baseX");
			System.out.println("Identified: baseY");
		}
	}
}
