package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class GameObjectAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public GameObjectAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// BoundaryObject
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("GameObject").isIdentified() && info.getClassInfo("Region").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				// rev 129 cr.r(IIIIIIIILct;IZII)Z 283

				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.NEW).capture().b());
				list.add(PatternElement.b().opcodes(Opcodes.DUP).b());
				list.add(PatternElement.b().opcodes(Opcodes.INVOKESPECIAL).b());
				list.add(PatternElement.b().opcodes(Opcodes.ASTORE).b());

				// objectId
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Flags
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Plane
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// WorldX
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// WorldY
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Height
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Render
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Orientation
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// LocalX
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// LocalY
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// OffsetX
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.IADD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.ISUB).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// OffsetY
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.IADD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.ISUB).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				Pattern pattern = new Pattern(list);

				ClassNode cn = info.getClassInfo("Region").getClassNode();

				List<PatternMatchResult> match = null;
				for (MethodNode mn : (List<MethodNode>) cn.methods) {
					if (!AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC) && Type.getReturnType(mn.desc).equals(Type.BOOLEAN_TYPE)) {

						// make sure method args are 8 integers and 2 non integers
						int iC = 0, bC = 0, oC = 0;
						for (Type t : Type.getArgumentTypes(mn.desc)) {
							if (t.equals(Type.INT_TYPE)) {
								iC++;
							} else if (t.equals(Type.BOOLEAN_TYPE)) {
								bC++;
							} else {
								oC++;
							}
						}

						if (iC == 11 && bC == 1 && oC == 1) {
							//System.out.println(mn.name);
							List<PatternMatchResult> matches = pattern.match(mn);
							if (matches.size() > 0) {
								if (match == null) {
									match = matches;
								} else {
									throw new IdentificationException("GameObject");
								}
							}
						}
					}
				}

				//System.out.println(match);
				if (match != null && match.size() == 1) {
					PatternMatchResult m = match.get(0);

					FieldInsnNode objectId = (FieldInsnNode) m.getCaptures().get(1);
					FieldInsnNode flags = (FieldInsnNode) m.getCaptures().get(2);
					FieldInsnNode plane = (FieldInsnNode) m.getCaptures().get(3);
					FieldInsnNode worldX = (FieldInsnNode) m.getCaptures().get(4);
					FieldInsnNode worldY = (FieldInsnNode) m.getCaptures().get(5);
					FieldInsnNode height = (FieldInsnNode) m.getCaptures().get(6);
					FieldInsnNode render = (FieldInsnNode) m.getCaptures().get(7);
					FieldInsnNode orientation = (FieldInsnNode) m.getCaptures().get(8);
					FieldInsnNode localX = (FieldInsnNode) m.getCaptures().get(9);
					FieldInsnNode localY = (FieldInsnNode) m.getCaptures().get(10);
					FieldInsnNode offsetX = (FieldInsnNode) m.getCaptures().get(11);
					FieldInsnNode offsetY = (FieldInsnNode) m.getCaptures().get(12);

					boolean success = true;
					success = success && objectId.desc.equals("I");
					success = success && flags.desc.equals("I");
					success = success && worldX.desc.equals("I");
					success = success && worldY.desc.equals("I");
					success = success && height.desc.equals("I");
					// success = success && render.desc.equals(""); // TODO make this check for the correct object type
					success = success && orientation.desc.equals("I");
					success = success && localX.desc.equals("I");
					success = success && localY.desc.equals("I");
					success = success && offsetX.desc.equals("I");
					success = success && offsetY.desc.equals("I");

					if (success) {
						FieldNode objectIdFn = AnalyzerUtil.getField(classes.get(objectId.owner), objectId.name);
						FieldNode flagsFn = AnalyzerUtil.getField(classes.get(flags.owner), flags.name);
						FieldNode planeFn = AnalyzerUtil.getField(classes.get(plane.owner), plane.name);
						FieldNode worldXFn = AnalyzerUtil.getField(classes.get(localX.owner), localX.name);
						FieldNode worldYFn = AnalyzerUtil.getField(classes.get(localX.owner), localX.name);
						FieldNode heightFn = AnalyzerUtil.getField(classes.get(height.owner), height.name);
						FieldNode renderFn = AnalyzerUtil.getField(classes.get(render.owner), render.name);
						FieldNode orientationFn = AnalyzerUtil.getField(classes.get(orientation.owner), orientation.name);
						FieldNode localXFn = AnalyzerUtil.getField(classes.get(localX.owner), localX.name);
						FieldNode localYFn = AnalyzerUtil.getField(classes.get(localY.owner), localY.name);
						FieldNode offsetXFn = AnalyzerUtil.getField(classes.get(offsetX.owner), offsetX.name);
						FieldNode offsetYFn = AnalyzerUtil.getField(classes.get(offsetY.owner), offsetY.name);

						info.getFieldInfo("GameObject", "objectId").identify(classes.get(objectId.owner), objectIdFn);
						info.getFieldInfo("GameObject", "flags").identify(classes.get(flags.owner), flagsFn);
						info.getFieldInfo("GameObject", "plane").identify(classes.get(plane.owner), planeFn);
						info.getFieldInfo("GameObject", "worldX").identify(classes.get(worldX.owner), worldXFn);
						info.getFieldInfo("GameObject", "worldY").identify(classes.get(worldY.owner), worldYFn);
						info.getFieldInfo("GameObject", "height").identify(classes.get(height.owner), heightFn);
						info.getFieldInfo("GameObject", "render").identify(classes.get(render.owner), renderFn);
						info.getFieldInfo("GameObject", "orientation").identify(classes.get(orientation.owner), orientationFn);
						info.getFieldInfo("GameObject", "localX").identify(classes.get(localX.owner), localXFn);
						info.getFieldInfo("GameObject", "localY").identify(classes.get(localY.owner), localYFn);
						info.getFieldInfo("GameObject", "offsetX").identify(classes.get(offsetX.owner), offsetXFn);
						info.getFieldInfo("GameObject", "offsetY").identify(classes.get(offsetY.owner), offsetYFn);

						if (print) {
							System.out.println("Identified: GameObject.objectId");
							System.out.println("Identified: GameObject.flags");
							System.out.println("Identified: GameObject.plane");
							System.out.println("Identified: GameObject.worldX");
							System.out.println("Identified: GameObject.worldY");
							System.out.println("Identified: GameObject.height");
							System.out.println("Identified: GameObject.render");
							System.out.println("Identified: GameObject.orientation");
							System.out.println("Identified: GameObject.localX");
							System.out.println("Identified: GameObject.localY");
							System.out.println("Identified: GameObject.offsetX");
							System.out.println("Identified: GameObject.offsetY");

						}

						TypeInsnNode classTn = (TypeInsnNode) m.getCaptures().get(0);
						ClassNode cn2 = classes.get(classTn.desc);

						info.getClassInfo("GameObject").identify(cn2);
						if (print)
							System.out.println("Identified: GameObject");
						return;
					} else {
						throw new IdentificationException("GameObject", "objectId");
					}
				} else {
					throw new IdentificationException("GameObject");
				}
			}

		});

	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("GameObject");
		classInfo.addFieldInfo(new FieldInfo("objectId"));
		classInfo.addFieldInfo(new FieldInfo("flags"));
		classInfo.addFieldInfo(new FieldInfo("localX"));
		classInfo.addFieldInfo(new FieldInfo("localY"));
		classInfo.addFieldInfo(new FieldInfo("plane"));
		classInfo.addFieldInfo(new FieldInfo("render"));
		classInfo.addFieldInfo(new FieldInfo("orientation"));
		classInfo.addFieldInfo(new FieldInfo("height"));
		classInfo.addFieldInfo(new FieldInfo("worldX"));
		classInfo.addFieldInfo(new FieldInfo("worldY"));
		classInfo.addFieldInfo(new FieldInfo("offsetX"));
		classInfo.addFieldInfo(new FieldInfo("offsetY"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("GameObject").isDone();
	}

}
