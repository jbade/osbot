package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class BoundaryObjectAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public BoundaryObjectAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// BoundaryObject
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("BoundaryObject").isIdentified() && info.getClassInfo("Region").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				// rev 129 cr.h(IIIILct;Lct;IIII)V

				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.NEW).capture().b());
				list.add(PatternElement.b().opcodes(Opcodes.DUP).b());
				list.add(PatternElement.b().opcodes(Opcodes.INVOKESPECIAL).b());
				list.add(PatternElement.b().opcodes(Opcodes.ASTORE).b());

				// objectId
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Flags
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// LocalX
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IADD).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// LocalY
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IADD).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Plane
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Render
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Render2
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Orientation
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				// Height
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				Pattern pattern = new Pattern(list);

				ClassNode cn = info.getClassInfo("Region").getClassNode();

				List<PatternMatchResult> match = null;
				for (MethodNode mn : (List<MethodNode>) cn.methods) {
					if (!AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC) && AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_PUBLIC)
							&& Type.getReturnType(mn.desc).equals(Type.VOID_TYPE)) {

						// make sure method args are 8 integers and 2 non integers
						int iC = 0, oC = 0;
						for (Type t : Type.getArgumentTypes(mn.desc)) {
							if (t.equals(Type.INT_TYPE)) {
								iC++;
							} else {
								oC++;
							}
						}

						if (iC == 8 && oC == 2) {
							List<PatternMatchResult> matches = pattern.match(mn);
							if (matches.size() > 0) {
								if (match == null) {
									match = matches;
								} else {
									throw new IdentificationException("BoundaryObject");
								}
							}
						}
					}
				}

				if (match != null && match.size() == 1) {
					PatternMatchResult m = match.get(0);

					FieldInsnNode objectId = (FieldInsnNode) m.getCaptures().get(1);
					FieldInsnNode flags = (FieldInsnNode) m.getCaptures().get(2);
					FieldInsnNode localX = (FieldInsnNode) m.getCaptures().get(3);
					FieldInsnNode localY = (FieldInsnNode) m.getCaptures().get(4);
					FieldInsnNode plane = (FieldInsnNode) m.getCaptures().get(5);
					FieldInsnNode render = (FieldInsnNode) m.getCaptures().get(6);
					FieldInsnNode render2 = (FieldInsnNode) m.getCaptures().get(7);
					FieldInsnNode orientation = (FieldInsnNode) m.getCaptures().get(8);
					FieldInsnNode height = (FieldInsnNode) m.getCaptures().get(9);

					boolean success = true;
					success = success && objectId.desc.equals("I");
					success = success && flags.desc.equals("I");
					success = success && localX.desc.equals("I");
					success = success && localY.desc.equals("I");
					success = success && plane.desc.equals("I");
					// success = success && render.desc.equals(""); // TODO make these check for the correct object type
					// success = success && render2.desc.equals("");
					success = success && orientation.desc.equals("I");
					success = success && height.desc.equals("I");

					if (success) {
						FieldNode idFn = AnalyzerUtil.getField(classes.get(objectId.owner), objectId.name);
						FieldNode flagsFn = AnalyzerUtil.getField(classes.get(flags.owner), flags.name);
						FieldNode localXFn = AnalyzerUtil.getField(classes.get(localX.owner), localX.name);
						FieldNode localYFn = AnalyzerUtil.getField(classes.get(localY.owner), localY.name);
						FieldNode planeFn = AnalyzerUtil.getField(classes.get(plane.owner), plane.name);
						FieldNode renderFn = AnalyzerUtil.getField(classes.get(render.owner), render.name);
						FieldNode render2Fn = AnalyzerUtil.getField(classes.get(render2.owner), render2.name);
						FieldNode orientationFn = AnalyzerUtil.getField(classes.get(orientation.owner), orientation.name);
						FieldNode heightFn = AnalyzerUtil.getField(classes.get(height.owner), height.name);

						info.getFieldInfo("BoundaryObject", "objectId").identify(classes.get(objectId.owner), idFn);
						info.getFieldInfo("BoundaryObject", "flags").identify(classes.get(flags.owner), flagsFn);
						info.getFieldInfo("BoundaryObject", "localX").identify(classes.get(localX.owner), localXFn);
						info.getFieldInfo("BoundaryObject", "localY").identify(classes.get(localY.owner), localYFn);
						info.getFieldInfo("BoundaryObject", "plane").identify(classes.get(plane.owner), planeFn);
						info.getFieldInfo("BoundaryObject", "render").identify(classes.get(render.owner), renderFn);
						info.getFieldInfo("BoundaryObject", "render2").identify(classes.get(render2.owner), render2Fn);
						info.getFieldInfo("BoundaryObject", "orientation").identify(classes.get(orientation.owner), orientationFn);
						info.getFieldInfo("BoundaryObject", "height").identify(classes.get(height.owner), heightFn);

						if (print) {
							System.out.println("Identified: BoundaryObject.objectId");
							System.out.println("Identified: BoundaryObject.flags");
							System.out.println("Identified: BoundaryObject.localX");
							System.out.println("Identified: BoundaryObject.localY");
							System.out.println("Identified: BoundaryObject.plane");
							System.out.println("Identified: BoundaryObject.render");
							System.out.println("Identified: BoundaryObject.render2");
							System.out.println("Identified: BoundaryObject.orientation");
							System.out.println("Identified: BoundaryObject.height");
						}

						TypeInsnNode classTn = (TypeInsnNode) m.getCaptures().get(0);
						ClassNode cn2 = classes.get(classTn.desc);

						info.getClassInfo("BoundaryObject").identify(cn2);
						
						if (print){
							System.out.println("Identified: BoundaryObject");							
						}
						
					} else {
						throw new IdentificationException("BoundaryObject", "objectId");	
					}
					
				} else {
					throw new IdentificationException("BoundaryObject");
				}

			}

		});

	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("BoundaryObject");
		classInfo.addFieldInfo(new FieldInfo("objectId"));
		classInfo.addFieldInfo(new FieldInfo("flags"));
		classInfo.addFieldInfo(new FieldInfo("localX"));
		classInfo.addFieldInfo(new FieldInfo("localY"));
		classInfo.addFieldInfo(new FieldInfo("plane"));
		classInfo.addFieldInfo(new FieldInfo("render"));
		classInfo.addFieldInfo(new FieldInfo("render2"));
		classInfo.addFieldInfo(new FieldInfo("orientation"));
		classInfo.addFieldInfo(new FieldInfo("height"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("BoundaryObject").isDone();
	}

}
