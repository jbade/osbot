package com.iinc.osupdater.analyzer.analyzers.collection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import com.iinc.osupdater.analyzer.analyzers.Analyzer;
import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;

public class LinkedListAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public LinkedListAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// LinkedList
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("LinkedList").isIdentified() && info.getClassInfo("Node").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print)
					throws IdentificationException {
				ClassNode node = info.getClassInfo("Node").getClassNode();
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals("java/lang/Object") && cn.interfaces.size() == 0
								&& AnalyzerUtil.countInstanceFields(cn, "L" + node.name + ";") == 2
								&& AnalyzerUtil.countInstanceFields(cn) == 2)
						.collect(Collectors.toList());

				// System.out.println(filtered.size());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("LinkedList").identify(cn);
					if (print)
						System.out.println("Identified: LinkedList");
				} else {
					throw new IdentificationException("LinkedList");
				}
			}

		});

		// head TODO
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("LinkedList").isIdentified() && info.getClassInfo("Node").isIdentified()
						&& !info.getFieldInfo("LinkedList", "head").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print)
					throws IdentificationException {
				ClassNode cn = info.getClassInfo("LinkedList").getClassNode();
				ClassNode node = info.getClassInfo("Node").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> fn.desc.equals(String.format("L%s;", node.name))
								&& AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_PUBLIC)
								&& !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					info.getFieldInfo("LinkedList", "head").identify(cn, filtered.get(0));
					if (print)
						System.out.println("Identified: LinkedList.head");
				} else {
					throw new IdentificationException("LinkedList", "head");
				} 
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("LinkedList");
		classInfo.addFieldInfo(new FieldInfo("head"));

		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("LinkedList").isDone();
	}

}
