package com.iinc.osupdater.analyzer.analyzers.collection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.analyzers.Analyzer;
import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;

public class DualNodeAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public DualNodeAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// DualNode
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("DualNode").isIdentified() && info.getClassInfo("Node").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Node").getClassNode().name)
								&& AnalyzerUtil.countInstanceFields(cn, "L" + cn.name + ";") == 2)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("DualNode").identify(cn);
					if (print)
						System.out.println("Identified: DualNode");
				} else {
					throw new IdentificationException("DualNode");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("DualNode");

		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("DualNode").isDone();
	}

}
