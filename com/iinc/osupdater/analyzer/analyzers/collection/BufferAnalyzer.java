package com.iinc.osupdater.analyzer.analyzers.collection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import com.iinc.osupdater.analyzer.analyzers.Analyzer;
import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;

public class BufferAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public BufferAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// Buffer
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("Buffer").isIdentified() && info.getClassInfo("Node").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Node").getClassNode().name)
								&& AnalyzerUtil.countInstanceFields(cn) == 2 && AnalyzerUtil.countInstanceFields(cn, "[B") == 1
								&& AnalyzerUtil.countInstanceFields(cn, "I") == 1)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("Buffer").identify(cn);
					if (print)
						System.out.println("Identified: Buffer");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("Buffer");
				}
			}
		});

		// position
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("Buffer").isIdentified() && !info.getFieldInfo("Buffer", "position").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Buffer").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> fn.desc.equals("I") && !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					info.getFieldInfo("Buffer", "position").identify(cn, filtered.get(0));
					if (print)
						System.out.println("Identified: Buffer.position");
				} else {
					throw new IdentificationException("Buffer", "position");
				}
			}

		});

		// payload
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("Buffer").isIdentified() && !info.getFieldInfo("Buffer", "payload").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Buffer").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> fn.desc.equals("[B") && !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					info.getFieldInfo("Buffer", "payload").identify(cn, filtered.get(0));
					if (print)
						System.out.println("Identified: Buffer.payload");
				} else {
					throw new IdentificationException("Buffer", "payload");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("Buffer");
		classInfo.addFieldInfo(new FieldInfo("position"));
		classInfo.addFieldInfo(new FieldInfo("payload"));

		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("Buffer").isDone();
	}

}