package com.iinc.osupdater.analyzer.analyzers.collection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import com.iinc.osupdater.analyzer.analyzers.Analyzer;
import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;

public class NodeAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public NodeAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// Node
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("Node").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals("java/lang/Object")
								&& AnalyzerUtil.countInstanceFields(cn, "L" + cn.name + ";") == 2
								&& AnalyzerUtil.countInstanceFields(cn, "J") == 1)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("Node").identify(cn);
					if (print)
						System.out.println("Identified: Node");
				} else {
					throw new IdentificationException("Node");
				}
			}

		});

		// id
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("Node").isIdentified() && !info.getFieldInfo("Node", "id").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Node").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> fn.desc.equals("J") && !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					info.getFieldInfo("Node", "id").identify(cn, filtered.get(0));
					if (print)
						System.out.println("Identified: Node.id");
				} else {
					throw new IdentificationException("Node", "id");
				}
			}

		});

		// next, prev
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("Node").isIdentified()
						&& (!info.getFieldInfo("Node", "next").isIdentified() || !info.getFieldInfo("Node", "prev").isIdentified());
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Node").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> fn.desc.equals("L" + cn.name + ";") && !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC))
						.collect(Collectors.toList());

				if (filtered.size() == 2) {
					for (FieldNode fn : filtered) {
						if (AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_PUBLIC)) {
							info.getFieldInfo("Node", "next").identify(cn, fn);
							if (print)
								System.out.println("Identified: Node.next");
						} else {
							info.getFieldInfo("Node", "prev").identify(cn, fn);
							if (print)
								System.out.println("Identified: Node.prev");
						}
					}

				} else {
					throw new IdentificationException("Node", "next");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("Node");
		classInfo.addFieldInfo(new FieldInfo("id"));
		classInfo.addFieldInfo(new FieldInfo("next"));
		classInfo.addFieldInfo(new FieldInfo("prev"));

		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("Node").isDone();
	}

}
