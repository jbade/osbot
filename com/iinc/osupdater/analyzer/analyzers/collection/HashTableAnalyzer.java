package com.iinc.osupdater.analyzer.analyzers.collection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.analyzers.Analyzer;
import com.iinc.osupdater.analyzer.analyzers.Identifier;
import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class HashTableAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public HashTableAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// HashTable
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("HashTable").isIdentified() && info.getClassInfo("Node").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals("java/lang/Object") && cn.interfaces.size() == 0
								&& AnalyzerUtil.countInstanceFields(cn, "L" + info.getClassInfo("Node").getClassNode().name + ";") == 2
								&& AnalyzerUtil.countInstanceFields(cn, "[L" + info.getClassInfo("Node").getClassNode().name + ";") == 1)
						.collect(Collectors.toList());

				for (ClassNode cn : filtered)
					System.out.println(cn.name);
				System.out.println(filtered.size());
				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("HashTable").identify(cn);
					if (print)
						System.out.println("Identified: HashTable");
				} else {
					throw new IdentificationException("HashTable");
				}
			}

		});

		// buckets
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("HashTable").isIdentified() && info.getClassInfo("Node").isIdentified()
						&& !info.getFieldInfo("HashTable", "buckets").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<FieldNode> filtered = ((List<FieldNode>) info.getClassInfo("HashTable").getClassNode().fields).stream()
						.filter((fn) -> fn.desc.equals("[L" + info.getClassInfo("Node").getClassNode().name + ";"))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					FieldNode fn = filtered.get(0);
					info.getFieldInfo("HashTable", "buckets").identify(info.getClassInfo("HashTable").getClassNode(), fn);
					if (print)
						System.out.println("Identified: HashTable.buckets");
				} else {
					throw new IdentificationException("HashTable", "buckets");
				}
			}

		});

		// index
		identifiers.add(new Identifier() {
			Pattern pattern;
			{
				// 118 ge.h()Lhs;
				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ICONST_0).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.INVOKEVIRTUAL).b());
				list.add(PatternElement.b().opcodes(Opcodes.ARETURN).b());
				pattern = new Pattern(list);
			}

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("HashTable").isIdentified() && info.getClassInfo("Node").isIdentified()
						&& !info.getFieldInfo("HashTable", "index").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("HashTable").getClassNode();
				List<MethodNode> filtered = ((List<MethodNode>) cn.methods).stream()
						.filter((mn) -> !AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC)
								&& mn.desc.equals("()L" + info.getClassInfo("Node").getClassNode().name + ";"))
						.collect(Collectors.toList());

				boolean found = false;

				FieldNode foundFn = null;

				for (MethodNode mn : filtered) {
					for (PatternMatchResult match : pattern.match(mn)) {
						FieldInsnNode fin = (FieldInsnNode) match.getCaptures().get(0);
						if (fin.desc.equals("I") && fin.owner.equals(cn.name)) {
							FieldNode fn = AnalyzerUtil.getField(cn, fin.name);
							if (fn != null) {
								if (found) {
									throw new IdentificationException("HashTable", "index");
								}

								found = true;
								foundFn = fn;
							}

						}
					}
				}

				if (foundFn != null) {
					info.getFieldInfo("HashTable", "index").identify(cn, foundFn);
					if (print)
						System.out.println("Identified: HashTable.index");
				} else {
					info.getFieldInfo("HashTable", "index").setIdentified(true);
				}

			}

		});

		// size
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("HashTable").isIdentified() && info.getFieldInfo("HashTable", "index").isIdentified()
						&& !info.getFieldInfo("HashTable", "size").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("HashTable").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC) && fn.desc.equals("I")
								&& !fn.name.equals(info.getFieldInfo("HashTable", "index").getFieldNode().name))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					info.getFieldInfo("HashTable", "size").identify(cn, filtered.get(0));
					if (print)
						System.out.println("Identified: HashTable.size");
				} else {
					throw new IdentificationException("HashTable", "size");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("HashTable");
		classInfo.addFieldInfo(new FieldInfo("buckets"));
		classInfo.addFieldInfo(new FieldInfo("index"));
		classInfo.addFieldInfo(new FieldInfo("size"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("HashTable").isDone();
	}

}