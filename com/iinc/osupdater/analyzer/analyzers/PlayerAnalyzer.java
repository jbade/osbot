package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.OpcodeGroups;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class PlayerAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public PlayerAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// Player
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("Player").isIdentified() && info.getClassInfo("Character").isIdentified()
						&& info.getClassInfo("Npc").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> !cn.name.equals(info.getClassInfo("Npc").getClassNode().name)
								&& cn.superName.equals(info.getClassInfo("Character").getClassNode().name))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("Player").identify(cn);
					if (print)
						System.out.println("Identified: Player");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("Player");
				}
			}
		});

		// combatLevel, skullIcon
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return (!info.getFieldInfo("Player", "combatLevel").isIdentified()
						|| !info.getFieldInfo("Player", "skullIcon").isIdentified()) && info.getClassInfo("Player").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Player").getClassNode();

				// 118 ad.l
				List<MethodNode> filtered = ((List<MethodNode>) cn.methods).stream().filter((mv) -> mv.name.equals("<init>"))
						.collect(Collectors.toList());

				if (filtered.size() != 1) {
					throw new IdentificationException("Player", "combatLevel");
				}

				MethodNode mn = filtered.get(0);
				// System.out.println(info.getClassInfo("Player").getClassNode().name + "." + mn.name);

				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());

				Pattern pattern = new Pattern(list);

				List<PatternMatchResult> matches = pattern.match(mn);

				// System.out.println(matches.size());
				// for (PatternMatchResult pmr : matches)
				// System.out.println(pmr);
				// System.exit(0);

				if (matches.size() != 10) {
					throw new IdentificationException("Player", "combatLevel");
				}

				FieldInsnNode finCombatLevel = (FieldInsnNode) matches.get(2).getCaptures().get(0);
				FieldInsnNode finSkullIcon = (FieldInsnNode) matches.get(0).getCaptures().get(0);

				FieldNode combatLevel = AnalyzerUtil.getField(cn, finCombatLevel.name);
				FieldNode skullIcon = AnalyzerUtil.getField(cn, finSkullIcon.name);

				if (!combatLevel.desc.equals("I") || !skullIcon.desc.equals("I")) {
					throw new IdentificationException("Player", "combatLevel");
				}

				info.getFieldInfo("Player", "combatLevel").identify(cn, combatLevel);
				info.getFieldInfo("Player", "skullIcon").identify(cn, skullIcon);

				if (print) {
					System.out.println("Identified: Player.combatLevel");
					System.out.println("Identified: Player.skullIcon");
				}
			}
		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("Player");
		classInfo.addFieldInfo(new FieldInfo("combatLevel"));
		classInfo.addFieldInfo(new FieldInfo("skullIcon"));

		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("Player").isDone();
	}

}
