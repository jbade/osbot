package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class WidgetNodeAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public WidgetNodeAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// WidgetNode
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("WidgetNode").isIdentified() && info.getClassInfo("Node").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Node").getClassNode().name)
								&& AnalyzerUtil.countInstanceFields(cn, "I") == 2 && AnalyzerUtil.countInstanceFields(cn, "Z") == 1
								&& AnalyzerUtil.countInstanceFields(cn) == 3)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);

					info.getClassInfo("WidgetNode").identify(cn);
					if (print)
						System.out.println("Identified: WidgetNode");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("WidgetNode");
				}
			}
		});

		// widgetNodeId
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("WidgetNode").isIdentified() && !info.getFieldInfo("WidgetNode", "widgetNodeId").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("WidgetNode").getClassNode();

				// rev 122 ck.dj(Lr;Z)V
				List<MethodNode> filtered = AnalyzerUtil.allStaticMethods(classes.values()).stream()
						.filter((mn) -> mn.desc.equals("(L" + cn.name + ";Z)V")).collect(Collectors.toList());

				// System.out.println(filtered.size());

				if (filtered.size() != 1) {
					throw new IdentificationException("WidgetNode", "widgetNodeId");
				}

				MethodNode mn = filtered.get(0);
				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.GETFIELD).capture().b());
				Pattern pattern = new Pattern(list);

				List<PatternMatchResult> matches = pattern.match(mn);
				// System.out.println(matches.size());

				if (matches.size() < 2 || matches.size() > 3) {
					throw new IdentificationException("WidgetNode", "widgetNodeId");
				}

				FieldInsnNode finId = (FieldInsnNode) matches.get(0).getCaptures().get(0);

				FieldNode id = AnalyzerUtil.getField(cn, finId.name);

				boolean desc = true;
				desc = desc && id.desc.equals("I");

				if (!desc) {
					throw new IdentificationException("WidgetNode", "widgetNodeId");
				}

				info.getFieldInfo("WidgetNode", "widgetNodeId").identify(cn, id);
				if (print) {
					System.out.println("Identified: WidgetNode.widgetNodeId");
				}
			}
		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("WidgetNode");
		classInfo.addFieldInfo(new FieldInfo("widgetNodeId"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("WidgetNode").isDone();
	}

}
