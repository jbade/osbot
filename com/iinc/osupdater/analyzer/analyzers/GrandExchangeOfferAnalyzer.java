package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class GrandExchangeOfferAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public GrandExchangeOfferAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// identify class
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("GrandExchangeOffer").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print)
					throws IdentificationException {
				List<ClassNode> filtered = classes
						.values().stream().filter((cn) -> cn.superName.equals("java/lang/Object")
								&& AnalyzerUtil.countInstanceFields(cn, "I") == 5 && AnalyzerUtil.countInstanceFields(cn, "B") == 1)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("GrandExchangeOffer").identify(cn);
					if (print)
						System.out.println("Identified: GrandExchangeOffer");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("GrandExchangeOffer");
				}
			}
		});

		// identify fields
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("GrandExchangeOffer").isIdentified() && info.getClassInfo("Buffer").isIdentified()
						&& !info.getFieldInfo("GrandExchangeOffer", "state").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print)
					throws IdentificationException {
				ClassNode cn = info.getClassInfo("GrandExchangeOffer").getClassNode();

				List<MethodNode> filtered = ((List<MethodNode>) cn.methods).stream().filter(
						(mn) -> mn.name.equals("<init>") && mn.desc.equals("(L" + info.getClassInfo("Buffer").getClassNode().name + ";Z)V"))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					MethodNode mn = filtered.get(0);
					// System.out.println(cn.name + " " + mn.name);
					List<PatternElement> list = new LinkedList<PatternElement>();
					list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());
					Pattern pattern = new Pattern(list);

					List<PatternMatchResult> matches = pattern.match(mn);
					if (matches.size() >= 6) {
						FieldInsnNode state = (FieldInsnNode) matches.get(0).getCaptures().get(0);
						FieldInsnNode itemId = (FieldInsnNode) matches.get(1).getCaptures().get(0);
						FieldInsnNode price = (FieldInsnNode) matches.get(2).getCaptures().get(0);
						FieldInsnNode quantity = (FieldInsnNode) matches.get(3).getCaptures().get(0);
						FieldInsnNode transfered = (FieldInsnNode) matches.get(4).getCaptures().get(0);
						FieldInsnNode spent = (FieldInsnNode) matches.get(5).getCaptures().get(0);

						info.getFieldInfo("GrandExchangeOffer", "state").identify(cn, AnalyzerUtil.getField(cn, state.name));
						info.getFieldInfo("GrandExchangeOffer", "itemId").identify(cn, AnalyzerUtil.getField(cn, itemId.name));
						info.getFieldInfo("GrandExchangeOffer", "price").identify(cn, AnalyzerUtil.getField(cn, price.name));
						info.getFieldInfo("GrandExchangeOffer", "quantity").identify(cn, AnalyzerUtil.getField(cn, quantity.name));
						info.getFieldInfo("GrandExchangeOffer", "transfered").identify(cn, AnalyzerUtil.getField(cn, transfered.name));
						info.getFieldInfo("GrandExchangeOffer", "spent").identify(cn, AnalyzerUtil.getField(cn, spent.name));

						if (print) {
							System.out.println("Identified: GrandExchangeOffer.state");
							System.out.println("Identified: GrandExchangeOffer.itemId");
							System.out.println("Identified: GrandExchangeOffer.price");
							System.out.println("Identified: GrandExchangeOffer.quantity");
							System.out.println("Identified: GrandExchangeOffer.transfered");
							System.out.println("Identified: GrandExchangeOffer.spent");
						}
					} else {
						throw new IdentificationException("GrandExchangeOffer", "state");
					}

				} else {
					throw new IdentificationException("GrandExchangeOffer", "state");
				} 
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("GrandExchangeOffer");
		classInfo.addFieldInfo(new FieldInfo("itemId"));
		classInfo.addFieldInfo(new FieldInfo("transfered"));
		classInfo.addFieldInfo(new FieldInfo("quantity"));
		classInfo.addFieldInfo(new FieldInfo("spent"));
		classInfo.addFieldInfo(new FieldInfo("price"));
		classInfo.addFieldInfo(new FieldInfo("state"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("GrandExchangeOffer").isDone();
	}

}