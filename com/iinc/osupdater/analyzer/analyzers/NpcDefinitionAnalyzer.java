package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class NpcDefinitionAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public NpcDefinitionAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// NpcDefinition, Npc.definition
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("NpcDefinition").isIdentified() && info.getClassInfo("Npc").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<FieldNode> filtered = ((List<FieldNode>) info.getClassInfo("Npc").getClassNode().fields).stream()
						.filter((fn) -> !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC)).collect(Collectors.toList());

				if (filtered.size() == 1) {
					FieldNode fn = filtered.get(0);
					ClassNode cn = classes.get(fn.desc.substring(0, fn.desc.length() - 1).substring(1)); // remove L; from Lclass;
					info.getClassInfo("NpcDefinition").identify(cn);
					info.getFieldInfo("Npc", "npcDefinition").identify(info.getClassInfo("Npc").getClassNode(), fn);
					if (print) {
						System.out.println("Identified: NpcDefinition");
						System.out.println("Identified: Npc.npcDefinition");
					}
				} else {
					throw new IdentificationException("NpcDefinition");
				}
			}

		});

		// name
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getFieldInfo("NpcDefinition", "name").isIdentified() && info.getClassInfo("NpcDefinition").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("NpcDefinition").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> fn.desc.equals("Ljava/lang/String;") && !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					FieldNode fn = filtered.get(0);
					info.getFieldInfo("NpcDefinition", "name").identify(cn, fn);
					if (print)
						System.out.println("Identified: NpcDefinition.name");
				} else {
					throw new IdentificationException("NpcDefinition", "name");
				}
			}
		});

		// npcId
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("NpcDefinition").isIdentified() && !info.getFieldInfo("NpcDefinition", "npcId").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("NpcDefinition").getClassNode();

				// rev 122 ck.dj(Lr;Z)V
				List<MethodNode> filtered = AnalyzerUtil.allStaticMethods(classes.values()).stream()
						.filter((mn) -> mn.desc.equals("(I)L" + cn.name + ";")).collect(Collectors.toList());

				// System.out.println(filtered.size());

				if (filtered.size() != 1) {
					throw new IdentificationException("NpcDefinition", "npcId");
				}

				MethodNode mn = filtered.get(0);
				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());
				Pattern pattern = new Pattern(list);

				List<PatternMatchResult> matches = pattern.match(mn);
				// System.out.println(matches.size());

				if (matches.size() != 1) {
					throw new IdentificationException("NpcDefinition", "npcId");
				}

				FieldInsnNode finId = (FieldInsnNode) matches.get(0).getCaptures().get(0);

				FieldNode id = AnalyzerUtil.getField(cn, finId.name);

				boolean desc = true;
				desc = desc && id.desc.equals("I");

				if (!desc) {
					throw new IdentificationException("NpcDefinition", "npcId");
				}

				info.getFieldInfo("NpcDefinition", "npcId").identify(cn, id);
				if (print) {
					System.out.println("Identified: NpcDefinition.npcId");
				}
			}
		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("NpcDefinition");
		classInfo.addFieldInfo(new FieldInfo("name"));
		classInfo.addFieldInfo(new FieldInfo("npcId"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("NpcDefinition").isDone();
	}
}