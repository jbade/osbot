package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;

public class RegionAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public RegionAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// Region
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("Region").isIdentified() && info.getClassInfo("Node").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				// Region contains a 3d array of a class that extends Node
				List<ClassNode> extendsNode = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Node").getClassNode().name)).collect(Collectors.toList());
				List<ClassNode> filtered = classes.values().stream().filter((cn) -> extendsNode.stream()
						.map((en) -> AnalyzerUtil.countInstanceFields(cn, "[[[L" + en.name + ";")).reduce((acc, i) -> acc + i).get() == 1)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("Region").identify(cn);
					if (print)
						System.out.println("Identified: Region");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("Region");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("Region");
		classInfo.addFieldInfo(new FieldInfo("sceneTiles")); // identified in SceneTile
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("Region").isDone();
	}

}
