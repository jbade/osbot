package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class CharacterAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public CharacterAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// Character
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("Character").isIdentified() && info.getClassInfo("Renderable").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Renderable").getClassNode().name)
								&& AnalyzerUtil.hasAccessFlags(cn, Opcodes.ACC_ABSTRACT))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("Character").identify(cn);
					if (print)
						System.out.println("Identified: Character");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("Character");
				}
			}
		});

		// localX, localY
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return (!info.getFieldInfo("Character", "localX").isIdentified() || !info.getFieldInfo("Character", "localY").isIdentified())
						&& info.getClassInfo("Character").isIdentified() && info.getClassInfo("Npc").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Character").getClassNode();

				// 118 ad.l
				List<MethodNode> filtered = ((List<MethodNode>) info.getClassInfo("Npc").getClassNode().methods).stream()
						.filter((mn) -> !AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC) && mn.desc.equals("(IIZ)V"))
						.collect(Collectors.toList());

				if (filtered.size() != 1) {
					throw new IdentificationException("Character", "localX");
				}

				MethodNode mn = filtered.get(0);
				// System.out.println(info.getClassInfo("Npc").getClassNode().name + "." + mn.name);

				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.GETFIELD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ICONST_0).b());
				list.add(PatternElement.b().opcodes(Opcodes.IALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.LDC).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.GETFIELD).b());
				list.add(PatternElement.b().opcodes(Opcodes.LDC).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.IADD).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.GETFIELD).b());
				list.add(PatternElement.b().opcodes(Opcodes.ICONST_0).b());
				list.add(PatternElement.b().opcodes(Opcodes.IALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.LDC).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
				list.add(PatternElement.b().opcodes(Opcodes.GETFIELD).b());
				list.add(PatternElement.b().opcodes(Opcodes.LDC).b());
				list.add(PatternElement.b().opcodes(Opcodes.IMUL).b());
				list.add(PatternElement.b().opcodes(Opcodes.IADD).b());
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());
				Pattern pattern = new Pattern(list);

				List<PatternMatchResult> matches = pattern.match(mn);
				if (matches.size() != 1) {
					throw new IdentificationException("Character", "localX");
				}

				FieldInsnNode finX = (FieldInsnNode) matches.get(0).getCaptures().get(0);
				FieldInsnNode finY = (FieldInsnNode) matches.get(0).getCaptures().get(1);

				FieldNode x = AnalyzerUtil.getField(cn, finX.name);
				FieldNode y = AnalyzerUtil.getField(cn, finY.name);

				info.getFieldInfo("Character", "localX").identify(cn, x);
				info.getFieldInfo("Character", "localY").identify(cn, y);

				if (print) {
					System.out.println("Identified: Character.localX");
					System.out.println("Identified: Character.localY");
				}
			}
		});

		// combatLevel, skullIcon
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return (!info.getFieldInfo("Character", "interactingIndex").isIdentified()
						|| !info.getFieldInfo("Character", "animation").isIdentified()) && info.getClassInfo("Character").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Character").getClassNode();

				// 118 ad.l
				List<MethodNode> filtered = ((List<MethodNode>) cn.methods).stream().filter((mv) -> mv.name.equals("<init>"))
						.collect(Collectors.toList());

				if (filtered.size() != 1) {
					throw new IdentificationException("Character", "interactingIndex");
				}

				MethodNode mn = filtered.get(0);
				// System.out.println(info.getClassInfo("Npc").getClassNode().name + "." + mn.name);

				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());
				Pattern pattern = new Pattern(list);

				List<PatternMatchResult> matches = pattern.match(mn);

				// System.out.println(matches.size());
				// for (PatternMatchResult pmr : matches)
				// System.out.println(pmr);

				if (matches.size() != 46) {
					throw new IdentificationException("Character", "interactingIndex");
				}

				FieldInsnNode finInteractingIndex = (FieldInsnNode) matches.get(22).getCaptures().get(0);
				FieldInsnNode finAnimation = (FieldInsnNode) matches.get(28).getCaptures().get(0);

				FieldNode interactingIndex = AnalyzerUtil.getField(cn, finInteractingIndex.name);
				FieldNode animation = AnalyzerUtil.getField(cn, finAnimation.name);

				boolean desc = true;
				desc = desc && interactingIndex.desc.equals("I");
				desc = desc && animation.desc.equals("I");

				if (!desc) {
					throw new IdentificationException("Character", "interactingIndex");
				}

				info.getFieldInfo("Character", "interactingIndex").identify(cn, interactingIndex);
				info.getFieldInfo("Character", "animation").identify(cn, animation);

				if (print) {
					System.out.println("Identified: Character.interactingIndex");
					System.out.println("Identified: Character.animation");
				}
			}
		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("Character");
		classInfo.addFieldInfo(new FieldInfo("localX"));
		classInfo.addFieldInfo(new FieldInfo("localY"));
		classInfo.addFieldInfo(new FieldInfo("interactingIndex"));
		classInfo.addFieldInfo(new FieldInfo("animation"));

		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("Character").isDone();
	}

}
