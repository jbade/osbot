package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.multiplier.MultiplierCollector;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class WidgetAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public WidgetAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// Widget
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("Widget").isIdentified() && info.getClassInfo("Node").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Node").getClassNode().name)
								&& AnalyzerUtil.countInstanceFields(cn) == 136)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);

					info.getClassInfo("Widget").identify(cn);
					if (print)
						System.out.println("Identified: Widget");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("Widget");
				}
			}

		});

		//
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("Widget").isIdentified() && !info.getFieldInfo("Widget", "itemId").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Widget").getClassNode();
				List<MethodNode> filtered = ((List<MethodNode>) cn.methods).stream()
						.filter((mn) -> mn.name.equals("<init>") && mn.desc.equals("()V")).collect(Collectors.toList());

				if (filtered.size() == 1) {
					MethodNode mn = filtered.get(0);

					List<PatternElement> list = new LinkedList<PatternElement>();
					list.add(PatternElement.b().opcodes(Opcodes.PUTFIELD).capture().b());
					Pattern pattern = new Pattern(list);

					List<PatternMatchResult> matches = pattern.match(mn);

					FieldInsnNode id = (FieldInsnNode) matches.get(1).getCaptures().get(0);
					FieldInsnNode absoluteX = (FieldInsnNode) matches.get(13).getCaptures().get(0);
					FieldInsnNode absoluteY = (FieldInsnNode) matches.get(14).getCaptures().get(0);
					FieldInsnNode width = (FieldInsnNode) matches.get(15).getCaptures().get(0);
					FieldInsnNode height = (FieldInsnNode) matches.get(16).getCaptures().get(0);
					FieldInsnNode parentId = (FieldInsnNode) matches.get(19).getCaptures().get(0);
					FieldInsnNode hidden = (FieldInsnNode) matches.get(20).getCaptures().get(0);
					FieldInsnNode relativeX = (FieldInsnNode) matches.get(21).getCaptures().get(0);
					FieldInsnNode relativeY = (FieldInsnNode) matches.get(22).getCaptures().get(0);
					FieldInsnNode textureId = (FieldInsnNode) matches.get(35).getCaptures().get(0);
					FieldInsnNode text = (FieldInsnNode) matches.get(58).getCaptures().get(0);
					FieldInsnNode name = (FieldInsnNode) matches.get(67).getCaptures().get(0);
					FieldInsnNode parent = (FieldInsnNode) matches.get(68).getCaptures().get(0);
					FieldInsnNode itemId = (FieldInsnNode) matches.get(77).getCaptures().get(0);
					FieldInsnNode itemQuantity = (FieldInsnNode) matches.get(78).getCaptures().get(0);
					FieldInsnNode boundsIndex = (FieldInsnNode) matches.get(87).getCaptures().get(0);

					FieldNode idFn = AnalyzerUtil.getField(cn, id.name);
					FieldNode absoluteXFn = AnalyzerUtil.getField(cn, absoluteX.name);
					FieldNode absoluteYFn = AnalyzerUtil.getField(cn, absoluteY.name);
					FieldNode widthFn = AnalyzerUtil.getField(cn, width.name);
					FieldNode heightFn = AnalyzerUtil.getField(cn, height.name);
					FieldNode parentIdFn = AnalyzerUtil.getField(cn, parentId.name);
					FieldNode hiddenFn = AnalyzerUtil.getField(cn, hidden.name);
					FieldNode relativeXFn = AnalyzerUtil.getField(cn, relativeX.name);
					FieldNode relativeYFn = AnalyzerUtil.getField(cn, relativeY.name);
					FieldNode textureIdFn = AnalyzerUtil.getField(cn, textureId.name);
					FieldNode textFn = AnalyzerUtil.getField(cn, text.name);
					FieldNode nameFn = AnalyzerUtil.getField(cn, name.name);
					FieldNode parentFn = AnalyzerUtil.getField(cn, parent.name);
					FieldNode itemIdFn = AnalyzerUtil.getField(cn, itemId.name);
					FieldNode itemQuantityFn = AnalyzerUtil.getField(cn, itemQuantity.name);
					FieldNode boundsIndexFn = AnalyzerUtil.getField(cn, boundsIndex.name);

					boolean desc = true;
					desc = desc && idFn.desc.equals("I");
					desc = desc && absoluteXFn.desc.equals("I");
					desc = desc && absoluteYFn.desc.equals("I");
					desc = desc && widthFn.desc.equals("I");
					desc = desc && heightFn.desc.equals("I");
					desc = desc && parentIdFn.desc.equals("I");
					desc = desc && hiddenFn.desc.equals("Z");
					desc = desc && relativeXFn.desc.equals("I");
					desc = desc && relativeYFn.desc.equals("I");
					desc = desc && textureIdFn.desc.equals("I");
					desc = desc && textFn.desc.equals("Ljava/lang/String;");
					desc = desc && nameFn.desc.equals("Ljava/lang/String;");
					desc = desc && parentFn.desc.equals(String.format("L%s;", cn.name));
					desc = desc && itemIdFn.desc.equals("I");
					desc = desc && itemQuantityFn.desc.equals("I");
					desc = desc && boundsIndexFn.desc.equals("I");

					if (!desc) {
						throw new IdentificationException("Widget", "id");
					}

					info.getFieldInfo("Widget", "widgetId").identify(cn, idFn);
					info.getFieldInfo("Widget", "absoluteX").identify(cn, absoluteXFn);
					info.getFieldInfo("Widget", "absoluteY").identify(cn, absoluteYFn);
					info.getFieldInfo("Widget", "width").identify(cn, widthFn);
					info.getFieldInfo("Widget", "height").identify(cn, heightFn);
					info.getFieldInfo("Widget", "parentId").identify(cn, parentIdFn);
					info.getFieldInfo("Widget", "hidden").identify(cn, hiddenFn);
					info.getFieldInfo("Widget", "relativeX").identify(cn, relativeXFn);
					info.getFieldInfo("Widget", "relativeY").identify(cn, relativeYFn);
					info.getFieldInfo("Widget", "textureId").identify(cn, textureIdFn);
					info.getFieldInfo("Widget", "text").identify(cn, textFn);
					info.getFieldInfo("Widget", "name").identify(cn, nameFn);
					info.getFieldInfo("Widget", "parent").identify(cn, parentFn);
					info.getFieldInfo("Widget", "itemId").identify(cn, itemIdFn);
					info.getFieldInfo("Widget", "itemQuantity").identify(cn, itemQuantityFn);
					info.getFieldInfo("Widget", "boundsIndex").identify(cn, boundsIndexFn);

					if (print) {
						System.out.println("Identified: Widget.widgetId");
						System.out.println("Identified: Widget.absoluteX");
						System.out.println("Identified: Widget.absoluteY");
						System.out.println("Identified: Widget.width");
						System.out.println("Identified: Widget.height");
						System.out.println("Identified: Widget.parentId");
						System.out.println("Identified: Widget.hidden");
						System.out.println("Identified: Widget.relativeX");
						System.out.println("Identified: Widget.relativeY");
						System.out.println("Identified: Widget.textureId");
						System.out.println("Identified: Widget.text");
						System.out.println("Identified: Widget.name");
						System.out.println("Identified: Widget.parent");
						System.out.println("Identified: Widget.itemId");
						System.out.println("Identified: Widget.itemQuantity");
						System.out.println("Identified: Widget.boundsIndex");
					}

					// attempt to get textureId multiplier
					try {
						AbstractInsnNode ain = textureId.getPrevious();
						// System.out.println(InsnPrinter.prettyprint(ain));
						if (ain instanceof LdcInsnNode) {
							LdcInsnNode lin = (LdcInsnNode) ain;
							int val = MultiplierCollector.unoverflowDivide(-1, (int) lin.cst);
							// System.out.println(val);
							info.getFieldInfo("Widget", "textureId").setMultiplier(val);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {
					throw new IdentificationException("Widget", "widgetId");
				}
			}

		});

		// itemIds, itemQuantities
		identifiers.add(new Identifier() {
			// 118 fi.o
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("Widget").isIdentified() && !info.getFieldInfo("Widget", "itemIds").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Widget").getClassNode();
				List<MethodNode> filtered = ((List<MethodNode>) cn.methods).stream()
						.filter((mn) -> !AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC) && mn.desc.equals("(II)V"))
						.collect(Collectors.toList());
				if (filtered.size() == 1) {

					MethodNode mn = filtered.get(0);
					List<PatternElement> list = new LinkedList<PatternElement>();
					list.add(PatternElement.b().opcodes(Opcodes.GETFIELD).capture().b());
					Pattern pattern = new Pattern(list);

					List<PatternMatchResult> matches = pattern.match(mn);

					FieldInsnNode itemIds = (FieldInsnNode) matches.get(0).getCaptures().get(0);
					FieldInsnNode itemQuantities = (FieldInsnNode) matches.get(4).getCaptures().get(0);

					FieldNode itemidsFn = AnalyzerUtil.getField(cn, itemIds.name);
					FieldNode itemQuantitiesFn = AnalyzerUtil.getField(cn, itemQuantities.name);

					info.getFieldInfo("Widget", "itemIds").identify(cn, itemidsFn);
					info.getFieldInfo("Widget", "itemQuantities").identify(cn, itemQuantitiesFn);

					if (print) {
						System.out.println("Identified: Widget.itemIds");
						System.out.println("Identified: Widget.itemQuantities");
					}

				} else {
					throw new IdentificationException("Widget", "itemIds");
				}
			}

		});

		// children
		identifiers.add(new Identifier() {
			// 118 fi.o
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("Widget").isIdentified() && !info.getFieldInfo("Widget", "children").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Widget").getClassNode();
				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream()
						.filter((fn) -> !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC) && fn.desc.equals("[L" + cn.name + ";"))
						.collect(Collectors.toList());
				if (filtered.size() == 1) {
					info.getFieldInfo("Widget", "children").identify(cn, filtered.get(0));

					if (print) {
						System.out.println("Identified: Widget.children");
					}

				} else {
					throw new IdentificationException("Widget", "children");
				}
			}

		});

		// actions
		identifiers.add(new Identifier() {
			// 118 fi.x
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("Widget").isIdentified() && !info.getFieldInfo("Widget", "actions").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Widget").getClassNode();
				List<MethodNode> filtered = ((List<MethodNode>) cn.methods).stream()
						.filter((mn) -> !AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC) && mn.desc.equals("(ILjava/lang/String;)V"))
						.collect(Collectors.toList());
				if (filtered.size() == 1) {
					MethodNode mn = filtered.get(0);
					List<PatternElement> list = new LinkedList<PatternElement>();
					list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
					list.add(PatternElement.b().opcodes(Opcodes.GETFIELD).capture().b());
					list.add(PatternElement.b().opcodes(Opcodes.ILOAD).b());
					list.add(PatternElement.b().opcodes(Opcodes.ALOAD).b());
					list.add(PatternElement.b().opcodes(Opcodes.AASTORE).b());
					Pattern pattern = new Pattern(list);

					List<PatternMatchResult> matches = pattern.match(mn);
					
					if (matches.size() == 1){
						FieldInsnNode fin = (FieldInsnNode) matches.get(0).getCaptures().get(0);
						FieldNode fn =AnalyzerUtil.getField(cn, fin.name);
						info.getFieldInfo("Widget", "actions").identify(cn, fn);
					} else {
						throw new IdentificationException("Widget", "actions");
					}
				} else {
					throw new IdentificationException("Widget", "actions");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("Widget");
		classInfo.addFieldInfo(new FieldInfo("widgetId"));
		classInfo.addFieldInfo(new FieldInfo("absoluteX"));
		classInfo.addFieldInfo(new FieldInfo("absoluteY"));
		classInfo.addFieldInfo(new FieldInfo("width"));
		classInfo.addFieldInfo(new FieldInfo("height"));
		classInfo.addFieldInfo(new FieldInfo("parentId"));
		classInfo.addFieldInfo(new FieldInfo("hidden"));
		classInfo.addFieldInfo(new FieldInfo("relativeX"));
		classInfo.addFieldInfo(new FieldInfo("relativeY"));
		classInfo.addFieldInfo(new FieldInfo("textureId"));
		classInfo.addFieldInfo(new FieldInfo("text"));
		classInfo.addFieldInfo(new FieldInfo("name"));
		classInfo.addFieldInfo(new FieldInfo("parent"));
		classInfo.addFieldInfo(new FieldInfo("itemId"));
		classInfo.addFieldInfo(new FieldInfo("itemQuantity"));
		classInfo.addFieldInfo(new FieldInfo("boundsIndex"));
		classInfo.addFieldInfo(new FieldInfo("itemIds"));
		classInfo.addFieldInfo(new FieldInfo("itemQuantities"));
		classInfo.addFieldInfo(new FieldInfo("children"));
		classInfo.addFieldInfo(new FieldInfo("actions"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("Widget").isDone();
	}
}