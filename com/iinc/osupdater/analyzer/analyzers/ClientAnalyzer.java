package com.iinc.osupdater.analyzer.analyzers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.analyzer.util.FullField;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class ClientAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public ClientAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// identify canvas
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return info.getClassInfo("client").isIdentified() && !info.getFieldInfo("client", "canvas").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<FullField> canvases = new ArrayList<>();

				String name = "client";
				while (name != null && !name.startsWith("java/")) {
					ClassNode cn = classes.get(name);
					for (FieldNode fn : (List<FieldNode>) cn.fields) {
						if (fn.desc.equals("Ljava/awt/Canvas;")) {
							canvases.add(new FullField(cn, fn));
						}
					}

					name = cn.superName;
				}

				if (canvases.size() == 1) {
					info.getFieldInfo("client", "canvas").identify(canvases.get(0).classNode, canvases.get(0).fieldNode);

					if (print) {
						System.out.println("Identified: client.canvas");
					}
				} else {
					throw new IdentificationException("client", "canvas");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("client");
		classInfo.addFieldInfo(new FieldInfo("canvas"));
		info.addClassInfo(classInfo);

		info.getClassInfo("client").identify(classes.get("client"));
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("client").isDone();
	}

}