package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;

public class NpcAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public NpcAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// Npc
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("Npc").isIdentified() && info.getClassInfo("Character").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Character").getClassNode().name)
								&& AnalyzerUtil.countInstanceFields(cn) == 1)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("Npc").identify(cn);
					if (print)
						System.out.println("Identified: Npc");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("Npc");
				}
			}

		});

		// definition is in NpcDefinitionAnalyzer
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("Npc");
		classInfo.addFieldInfo(new FieldInfo("npcDefinition"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("Npc").isDone();
	}
}
