package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;

public interface Identifier {
	public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes);

	public void identify(Info info, HashMap<String, ClassNode> classes, boolean print)  throws IdentificationException;
}
