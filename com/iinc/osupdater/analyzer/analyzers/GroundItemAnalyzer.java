package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;
import com.iinc.osupdater.util.pattern.Pattern;
import com.iinc.osupdater.util.pattern.PatternElement;
import com.iinc.osupdater.util.pattern.PatternMatchResult;

public class GroundItemAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public GroundItemAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// GroundItem
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("GroundItem").isIdentified() && info.getClassInfo("Renderable").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("Renderable").getClassNode().name)
								&& AnalyzerUtil.countInstanceFields(cn, "I") == 2)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("GroundItem").identify(cn);
					if (print)
						System.out.println("Identified: GroundItem");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("GroundItem");
				}
			}
		});

		// itemId, stackSize
		identifiers.add(new Identifier() {
			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return (!info.getFieldInfo("GroundItem", "itemId").isIdentified()
						|| !info.getFieldInfo("GroundItem", "stackSize").isIdentified()) && info.getClassInfo("GroundItem").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("GroundItem").getClassNode();

				// 122 ap.v
				List<MethodNode> filtered = ((List<MethodNode>) cn.methods).stream()
						.filter((mn) -> !AnalyzerUtil.hasAccessFlags(mn, Opcodes.ACC_STATIC) && mn.desc.startsWith("()L"))
						.collect(Collectors.toList());

				if (filtered.size() != 1) {
					throw new IdentificationException("GroundItem", "itemId");
				}

				MethodNode mn = filtered.get(0);
				// System.out.println(info.getClassInfo("Npc").getClassNode().name + "." + mn.name);

				List<PatternElement> list = new LinkedList<PatternElement>();
				list.add(PatternElement.b().opcodes(Opcodes.GETFIELD).capture().b());
				Pattern pattern = new Pattern(list);

				List<PatternMatchResult> matches = pattern.match(mn);
				if (matches.size() != 2) {
					throw new IdentificationException("GroundItem", "itemId");
				}

				FieldInsnNode finItemId = (FieldInsnNode) matches.get(0).getCaptures().get(0);
				FieldInsnNode finStackSize = (FieldInsnNode) matches.get(1).getCaptures().get(0);

				FieldNode itemId = AnalyzerUtil.getField(cn, finItemId.name);
				FieldNode stackSize = AnalyzerUtil.getField(cn, finStackSize.name);

				info.getFieldInfo("GroundItem", "itemId").identify(cn, itemId);
				info.getFieldInfo("GroundItem", "stackSize").identify(cn, stackSize);

				if (print) {
					System.out.println("Identified: GroundItem.itemId");
					System.out.println("Identified: GroundItem.stackSize");
				}
			}
		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("GroundItem");
		classInfo.addFieldInfo(new FieldInfo("itemId"));
		classInfo.addFieldInfo(new FieldInfo("stackSize"));

		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("GroundItem").isDone();
	}

}
