package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.ClassInfo;
import com.iinc.osupdater.analyzer.structure.FieldInfo;
import com.iinc.osupdater.analyzer.structure.Info;
import com.iinc.osupdater.analyzer.util.AnalyzerUtil;

public class RenderableAnalyzer implements Analyzer {

	private LinkedList<Identifier> identifiers;

	public RenderableAnalyzer() {
		identifiers = new LinkedList<Identifier>();

		// Renderable
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getClassInfo("Renderable").isIdentified() && info.getClassInfo("DualNode").isIdentified();
			}

			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				List<ClassNode> filtered = classes.values().stream()
						.filter((cn) -> cn.superName.equals(info.getClassInfo("DualNode").getClassNode().name)
								&& AnalyzerUtil.hasAccessFlags(cn, Opcodes.ACC_ABSTRACT) && AnalyzerUtil.countInstanceFields(cn) == 1
								&& AnalyzerUtil.countInstanceFields(cn, "I") == 1)
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					ClassNode cn = filtered.get(0);
					info.getClassInfo("Renderable").identify(cn);
					if (print)
						System.out.println("Identified: Renderable");
				} else {
					for (ClassNode cn : filtered)
						System.out.println(cn.name);
					throw new IdentificationException("Renderable");
				}
			}

		});

		// modelHeight
		identifiers.add(new Identifier() {

			@Override
			public boolean shouldIdentify(Info info, HashMap<String, ClassNode> classes) {
				return !info.getFieldInfo("Renderable", "modelHeight").isIdentified() && info.getClassInfo("Renderable").isIdentified();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void identify(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {
				ClassNode cn = info.getClassInfo("Renderable").getClassNode();

				List<FieldNode> filtered = ((List<FieldNode>) cn.fields).stream().filter((fn) -> fn.desc.equals("I")
						&& !AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_STATIC) && AnalyzerUtil.hasAccessFlags(fn, Opcodes.ACC_PUBLIC))
						.collect(Collectors.toList());

				if (filtered.size() == 1) {
					FieldNode fn = filtered.get(0);
					info.getFieldInfo("Renderable", "modelHeight").identify(cn, fn);
					if (print)
						System.out.println("Identified: Renderable.modelHeight");
				} else {
					throw new IdentificationException("Renderable", "modelHeight");
				}
			}

		});
	}

	@Override
	public void initialize(Info info, HashMap<String, ClassNode> classes) {
		ClassInfo classInfo = new ClassInfo("Renderable");
		classInfo.addFieldInfo(new FieldInfo("modelHeight"));
		info.addClassInfo(classInfo);
	}

	@Override
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException {

		for (Identifier i : identifiers) {
			if (i.shouldIdentify(info, classes))
				i.identify(info, classes, print);
		}

		return info.getClassInfo("Renderable").isDone();
	}

}
