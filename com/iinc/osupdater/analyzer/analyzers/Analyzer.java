package com.iinc.osupdater.analyzer.analyzers;

import java.util.HashMap;

import org.objectweb.asm.tree.ClassNode;

import com.iinc.osupdater.analyzer.exceptions.IdentificationException;
import com.iinc.osupdater.analyzer.structure.Info;


public interface Analyzer {
	
	/**
	 * Must initialize info with all classes/fields.
	 * @param info
	 * @param classes
	 */
	public void initialize(Info info, HashMap<String, ClassNode> classes);
	
	/**
	 * 
	 * @return true: no more work to be done </br>
	 *         false: potentially more work to be done
	 */
	public boolean run(Info info, HashMap<String, ClassNode> classes, boolean print) throws IdentificationException;

}
